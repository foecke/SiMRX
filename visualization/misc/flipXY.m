% FLIPXY mirrors in Y direction and swaps X and Y coordinates afterwards.
%
% In SiMRX voxel information (e.g. the voxelGrid) is stored in row-mayor
% layout. In Matlab information are stored in column-mayor order, hence to
% process the data properly the row and columns have to swapped
% (transposed).
%
% Furthermore for plots, the information has to flipped in the y axis,
% which traslates to flipud in matlab


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function vol = flipXY(vol)

vol = permute(flip(vol,2), [2,1,3]);


end