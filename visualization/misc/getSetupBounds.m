% GETSETUPBOUNDS derives the effective region of interest that contains all
% setups entities.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021

function pROI = getSetupBounds(setup, config, border)

if nargin<2 || isempty(config)
    if isfield(setup, 'coils')
        coilsActive     = setup.coils;
    else
        coilsActive     = [];
    end
    if isfield(setup, 'sensors')
        sensorsActive   = setup.sensors;
    else
        sensorsActive     = [];
    end
else
    if isfield(setup, 'coils')
        coilsActive     = setup.coils(config.coilsActive);
    else
        coilsActive     = [];
    end
    if isfield(setup, 'sensors')
        sensorsActive   = setup.sensors(config.sensorsActive);
    else
        sensorsActive     = [];
    end
end

if nargin<3
    border = 0.1;
end

roiMin  = [setup.roi.x(1), setup.roi.y(1), setup.roi.z(1)];
roiMax  = [setup.roi.x(2), setup.roi.y(2), setup.roi.z(2)];

if isfield(setup, 'coils')
    for i=1:numel(coilsActive)
        curElement  = coilsActive(i);
        if isfield(curElement, 'Segments')
            roiMax  = bsxfun(@max,roiMax,max(curElement.Segments));
            roiMin  = bsxfun(@min,roiMin,min(curElement.Segments));
        else
            roiMax  = bsxfun(@max,roiMax,curElement.Position);
            roiMin  = bsxfun(@min,roiMin,curElement.Position);
        end
    end
end

if isfield(setup, 'sensors')
    for i=1:numel(sensorsActive)
        curElement  = sensorsActive(i);
        if isfield(curElement, 'Segments')
            roiMax  = bsxfun(@max,roiMax,max(curElement.Segments));
            roiMin  = bsxfun(@min,roiMin,min(curElement.Segments));
        else
            roiMax  = bsxfun(@max,roiMax,curElement.Position);
            roiMin  = bsxfun(@min,roiMin,curElement.Position);
        end
    end
end

borderAux  = border*max(range([roiMin;roiMax]));

pROI.x  = [roiMin(1) - borderAux, roiMax(1) + borderAux];
pROI.y  = [roiMin(2) - borderAux, roiMax(2) + borderAux];
pROI.z  = [roiMin(3) - borderAux, roiMax(3) + borderAux];


end

