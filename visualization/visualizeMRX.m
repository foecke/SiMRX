 %VISUALIZEMRX is a visualization tool for MRX setups.
%	[h, fid] = VISUALIZEMRX(setup) visualizes a given setup. 2D and 3D
%   setups are supported. fid is the respective figure identifier. h is a
%   struct that contains all used plot handles.
%
%	[h, fid] = VISUALIZEMRX(setup, varargin) allows optional
%   settings/parameter:
%       'config'        loaded MRX-config that is compatible with the given
%                       MRX-setup. This sets a currentPattern for a given
%                       set of coils, the set of used coils (coilsActive)
%                       and set of used sensors (sensorsActive).
%       'coilsActive'   list of coil IDs, which defines which coils are
%                       visualized. Accepted inputs: 'all' (default) or an
%                       array of coil indices.
%       'sensorsActive' list of sensor IDs, which defines which coils are
%                       visualized. Accepted inputs: 'all' (default) or an
%                       array of sensor indices.
%       'roi'           specifies the region of interest. If no region of
%                       interest is given it defaults to
%                       	roi.x = [0,1];
%                       	roi.y = [0,1];
%                       and
%                       	roi.z = [0,0]; (in 2D)
%                       	roi.z = [0,1]; (in 3D)
%       'res'           specifies the resolution of given volume data or
%                       given field data. If volume data are given the
%                       resolution is determined automatically.
%       'fields'        cell array that contains magnetic fields for every
%                       coil given in coilsActive or config.coilsActive
%                       respectively.
%       'fieldsNormal'  toggle if the shown fields are normalized
%       'volume'        volume data shown in the region of interest
%       'clim'          set custom color limits. By default the color
%                       limit is defined by the minumum and maximum value
%                       in the provided volume.
%       'cuts'          struct with x,y and z fields that specifies a
%                       subsection of the region of interest that is
%                       displayed. The values refer to the corresponding
%                       resolution of the volume data.
%       'showROI'       toggle to show the region of interest:
%                           0       region of interest is not shown
%                           1       region of interest is drawn (default)
%                           2       outline of region of interest is shown
%       'showAxis'      toggle to set axis visualization:
%                           0       axis is not shown
%                           1       axis is shown (default)
%                           2       axis is not shown, whitespace is
%                                   removed
%                           3       axis is shown, whitespace is removed
%       'showCoils'     toggle to set coil visualization: 
%                           0       coils are not shown
%                           1       all coils are shown (default)
%                           2       all coils are shown, active coil is
%                                   highlighted
%                           3       only active coil is shown
%       'showVolume'    toggle to set volume visualization: 
%                           0       volume is not shown
%                           1       volume grid is shown
%                           2       volume is shown
%                           3       magnetic field volume (if available)
%                           4       magnetic field volume (log, if avail.)
%       'showVolumeGrid' toggle to set gridlines when visualizing a volume:
%                           0       grid lines are not shown (default)
%                           1       grid lines are shown
%       'showPattern'   toggle to set pattern visualization mode: 
%                           0       activation is shown coilwise (default)
%                           1       activation is shown patternwise
%       'showFields'    toggle to set field visualization: 
%                           0       magnetic fields are not shown (default)
%                           1       magnetic fields are shown
%       'showSensors'   toggle to set sensor visualization: 
%                           0       sensors are not shown
%                           1       sensors are shown (default)
%       'drawCoilID'    preset a coil ID to be visualized
%       'drawPatternID' preset a pattern ID to be visualized
%       'fid'           figure handle to use for visualization
%       'view'          define azimuth and elevation angles
%       'axisLimits'    struct with x,y and z fields that specifies the
%                       axis limits for each dimension.
%       'coilArrowScale' integer to scale the coil arrow size used by
%                       quiver.
%       'sensorArrowScale' integer to scale the sensor arrow size used by
%                       quiver.
%       'sensorArrowScale' integer to scale the sensor arrow size used by
%                       quiver.
%       'fieldArrowScale' integer to scale the field arrow size used by
%                       quiver.
%       'roiDrawProperties' properties used by patch to render the ROI.
%                       See documentation for 'Patch Properties'
%       'sensorDrawProperties' properties used by quiver to draw the sensors.
%                       See documentation for 'Quiver Properties'
%       'coilDrawProperties' properties to draw the coils.
%                       See documentation for 'Line Properties'
%       'coilDrawPropertiesLine' line properties which are applied in the
%                       coil loop case.
%                       See documentation for 'Line Properties'
%       'coilDrawPropertiesArrow' line properties which are applied in the
%                       simple coil case.
%                       See documentation for 'Quiver Properties'
%       'coilLiveDrawProperties' variant of coilDrawProperties for a live
%                       coil. See 'coilDrawProperties'
%       'coilLiveDrawPropertiesLine' variant of coilDrawPropertiesLine for
%                       a live coil. See 'coilDrawPropertiesLine'
%       'coilLiveDrawPropertiesArrow' variant of coilDrawPropertiesArrow
%                       for a live coil. See 'coilDrawPropertiesArrow'
%       'fieldDrawProperties' line properties which are applied
%                       to the drawn field lines.
%                       See documentation for 'Quiver Properties'
%       'verbose'       verbose level based on logger


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function [h, fid] = visualizeMRX(varargin)


%% run example?
if nargin == 0
    [h, fid] = runExample;
    return;
end

presets = loadVisPresets();


%% parse inputs
p = inputParser;
p.addOptional('setup'                           , []    , @isstruct);

p.addParameter('config'                         , []    , @isstruct);
p.addParameter('coilsActive'                    , 'all' , @(s) any([strcmp({'all'}, s), ismatrix(s)]));
p.addParameter('sensorsActive'                  , 'all' , @(s) any([strcmp({'all'}, s), ismatrix(s)]));
p.addParameter('roi'                            , []    , @isstruct);
p.addParameter('res'                            , []    , @isnumeric);

p.addParameter('fields'                         , []);
p.addParameter('fieldsNormal'                   , 1     , @(s) ismember(s, 0:1));

p.addParameter('volume'                         , []);
p.addParameter('clim'                           , []    , @(s) (isnumeric(s) && numel(s)==2));
p.addParameter('cuts'                           , []    , @isstruct);

p.addParameter('export'                         , []);
p.addParameter('exportOverride'                 , false , @islogical);
p.addParameter('exportRenderer'                 , 0     , @(s) ismember(s, 0:1));
p.addParameter('exportFormat'                   , 'pdf' , @(s) any(strcmp({'pdf', 'PDF', '-dpdf', 'PNG', 'png', '-dpng', 'JPG', 'jpg', 'JPEG', 'jpeg', '-djpeg', 'SVG', 'svg', '-dsvg'}, s)));

p.addParameter('showROI'                        , 1     , @(s) ismember(s, 0:2));
p.addParameter('showAxis'                       , 1     , @(s) ismember(s, 0:3));
p.addParameter('showAxisRegion'                 , 0     , @(s) ismember(s, 0:1));
p.addParameter('showCoils'                      , 1     , @(s) ismember(s, 0:3));
p.addParameter('showVolume'                     , 2     , @(s) ismember(s, 0:4));
p.addParameter('showVolumeGrid'                 , 0     , @(s) ismember(s, 0:1));
p.addParameter('showPattern'                    , 0     , @(s) ismember(s, 0:1));
p.addParameter('showFields'                     , 0     , @(s) ismember(s, 0:1));
p.addParameter('showSensors'                    , 1     , @(s) ismember(s, 0:1));

p.addParameter('drawCoilID'                     , 1         , @isnumeric);
p.addParameter('drawPatternID'                  , 1         , @isnumeric);

p.addParameter('fid'                            , gcf);
p.addParameter('view'                           , [] , @(s) (isnumeric(s) && numel(s)==2));
p.addParameter('axisLimits'                     , []    , @isstruct);

p.addParameter('coilArrowScale'                 , presets.coilArrowScale        , @isnumeric);
p.addParameter('coilActiveArrowScale'           , presets.coilActiveArrowScale 	, @isnumeric);
p.addParameter('sensorArrowScale'               , presets.sensorArrowScale      , @isnumeric);
p.addParameter('fieldArrowScale'                , presets.fieldArrowScale       , @isnumeric);

p.addParameter('axisRegionDrawProperties'       , {});
p.addParameter('roiDrawProperties'              , {});
p.addParameter('sensorDrawProperties'           , {});
p.addParameter('coilDrawProperties'             , {});
p.addParameter('coilDrawPropertiesLine'         , {});
p.addParameter('coilDrawPropertiesArrow'        , {});
p.addParameter('coilLiveDrawProperties'         , {});
p.addParameter('coilLiveDrawPropertiesLine'     , {});
p.addParameter('coilLiveDrawPropertiesArrow'    , {});
p.addParameter('fieldDrawProperties'            , {});

p.addParameter('verbose'                        , 1         , @(s) ismember(s, 0:3));


p.parse(varargin{:});

changedParameter                = ismember(p.Parameters, setdiff(p.Parameters,p.UsingDefaults));

fid                             = p.Results.fid;
res                             = p.Results.res;
roi                             = p.Results.roi;
setup                           = p.Results.setup;
config                          = p.Results.config;
volumeCustom                    = p.Results.volume;
fields                          = p.Results.fields;
fieldsNormal                    = p.Results.fieldsNormal;
cuts                            = p.Results.cuts;
climCustom                      = p.Results.clim;
customView                      = p.Results.view;
axisLimits                      = p.Results.axisLimits;
exportFileName                  = p.Results.export;
exportOverride                  = p.Results.exportOverride;
exportRenderer                  = p.Results.exportRenderer;
exportFileFormat                = p.Results.exportFormat;
showROI                         = p.Results.showROI;
showAxis                        = p.Results.showAxis;
showAxisRegion                  = p.Results.showAxisRegion;
showCoils                       = p.Results.showCoils;
showVolume                      = p.Results.showVolume;
showVolumeGrid                  = p.Results.showVolumeGrid;
showPattern                     = p.Results.showPattern;
showFields                      = p.Results.showFields;
showSensors                     = p.Results.showSensors;
coilsActiveArg                  = p.Results.coilsActive;
sensorsActiveArg                = p.Results.sensorsActive;
coilArrowScale                  = p.Results.coilArrowScale;
coilActiveArrowScale            = p.Results.coilActiveArrowScale;
sensorArrowScale                = p.Results.sensorArrowScale;
fieldArrowScale                 = p.Results.fieldArrowScale;

axisRegionDrawProperties        = [presets.axisRegionDrawProperties, p.Results.axisRegionDrawProperties];
roiDrawProperties               = [presets.roiDrawProperties, p.Results.roiDrawProperties];
sensorDrawProperties            = [presets.sensorDrawProperties, p.Results.sensorDrawProperties];
coilDrawProperties              = [presets.coilDrawProperties, p.Results.coilDrawProperties];
coilDrawPropertiesLine          = [presets.coilDrawPropertiesLine, p.Results.coilDrawPropertiesLine];
coilDrawPropertiesArrow         = [presets.coilDrawPropertiesArrow, p.Results.coilDrawPropertiesArrow];
coilLiveDrawProperties          = [presets.coilLiveDrawProperties, p.Results.coilLiveDrawProperties];
coilLiveDrawPropertiesLine      = [presets.coilLiveDrawPropertiesLine, p.Results.coilLiveDrawPropertiesLine];
coilLiveDrawPropertiesArrow     = [presets.coilLiveDrawPropertiesArrow, p.Results.coilLiveDrawPropertiesArrow];
fieldDrawProperties             = [presets.fieldDrawProperties, p.Results.fieldDrawProperties];


drawCoilID                      = p.Results.drawCoilID;
drawPatternID                   = p.Results.drawPatternID;

verbose                         = p.Results.verbose;

logg                            = logger(verbose);

cutMode                         = 'Max'; % 'Min', 'Both'
cutMovable                      = false;
cutDir                          = 'z';


% assume reasonable resolution
if isempty(volumeCustom) || ~isempty(res)
    res = [res, 1, 1, 1];
else
    res = [size(volumeCustom), 1, 1, 1];
end
res = res(1:3);

if isempty(cuts)
    cuts.x = [1, res(1)];
    cuts.y = [1, res(2)];
    cuts.z = [1, res(3)];
end

if isempty(roi)
    if ~isempty(setup)
        roi = setup.roi;
    else
        roi.x = [0,1];
        roi.y = [0,1];
        if is3D
            roi.z = [0,1];
        else
            roi.z = [0,0];
        end
    end
end

if ~isempty(fields)
    [fieldGrid, fieldCutMap]    = createVoxelGrid(roi, res, 'b', cuts);
else
    fieldGrid = [];
    fieldCutMap = [];
end


if ~isempty(config)
    coilsActive     = config.coilsActive;
    sensorsActive   = config.sensorsActive;
    numOfPatterns   = size(config.currentPattern, 1);
elseif ~isempty(setup)
    if isfield(setup, 'coils')
        coilsActive     = 1:numel(setup.coils);
    else
        coilsActive     = [];
    end
    if isfield(setup, 'sensors')
        sensorsActive   = 1:numel(setup.sensors);
    else
        sensorsActive   = [];
    end
    numOfPatterns   = [];
else
    coilsActive     = [];
    sensorsActive   = [];
    numOfPatterns   = [];
end
if strcmp({'all'}, coilsActiveArg)
    coilsActiveArg = coilsActive;
end
if strcmp({'all'}, sensorsActiveArg)
    sensorsActiveArg = sensorsActive;
end
coilsActive     = coilsActive(ismember(coilsActive, coilsActiveArg));
sensorsActive   = sensorsActive(ismember(sensorsActive, sensorsActiveArg));

if isfield(config, 'coilsActive')
    config.coilsActive      = config.coilsActive(ismember(config.coilsActive, coilsActive));
end
if isfield(config, 'sensorsActive')
    config.sensorsActive    = config.sensorsActive(ismember(config.sensorsActive, sensorsActive));
end
if isfield(config, 'currentPattern')
    config.currentPattern   = config.currentPattern(:,ismember(config.coilsActive, coilsActive));
end


numOfCoils          = numel(coilsActive);

coilsLive         	= drawCoilID;
coilsNotLive     	= coilsActive;
coilsNotLive(ismember(coilsNotLive, coilsLive)) = [];

effectiveField      = [];
volume              = [];
clim                = [];


%% validate figure and set figure handles
if isOctave()
    if isempty(fid)||~ishandle(fid)
        fid = gcf;
    end
else
    if isempty(fid)||~ishandle(fid)||~isvalid(fid)
        fid = gcf;
    end
end

set(0, 'CurrentFigure', fid);
if ~isOctave()
    fid.WindowKeyPressFcn = @buttonPressFcn;
end
funcResetView();


%% define handles
h           = struct;
h.roi       = [];
h.sensors   = [];
h.coils     = [];
h.volume    = [];
h.fields    = [];


%% set toggle states

toggleROI           = [];
toggleAxis          = [];
toggleCoils         = [];
toggleVolume        = [];
toggleFields        = [];
togglePattern       = [];
toggleGrid          = [];
toggleSensors       = [];
toggleNormal        = [];
toggleCutMode       = [];
toggleCutMovable    = [];
toggleClim          = [];

logg.print(1, 'Initialize Settings');
logg.print(1, '===================\n');

switch showROI
    case 1
        toggleROIOn;
    case 2
        toggleROIGrid;
    otherwise
        toggleROIOff;
end

funcUpdateEffectiveField();
switch showVolume
    case 1
        toggleVolumeGrid;
    case 2
        if ~isempty(volumeCustom)
            toggleVolumeOn;
        else
            toggleVolumeOff;
        end
    case 3
        if ~isempty(fields)
            toggleVolumeField;
        else
            toggleVolumeOff;
        end
    case 4
        if ~isempty(fields)
            toggleVolumeFieldLog;
        else
            toggleVolumeOff;
        end
    otherwise
        toggleVolumeOff;
end

toggleCutModeMax;
toggleCutMovableFalse;
if ~isempty(climCustom)
    toggleClimOn;
else
    toggleClimOff;
end

switch showCoils
    case 1
        toggleCoilsOn;
    case 2
        toggleCoilsLive;
    case 3
        toggleCoilsLiveOnly;
    otherwise
        toggleCoilsOff;
end
if showFields
    toggleFieldsOn;
else
    toggleFieldsOff;
end
if showPattern
    togglePatternOn;
else
    togglePatternOff
end
if showVolumeGrid
    toggleGridOn;
else
    toggleGridOff;
end
if showSensors
    toggleSensorsOn;
else
    toggleSensorsOff;
end
if fieldsNormal
    toggleNormalOn;
else
    toggleNormalOff;
end

switch showAxis
    case 1
        toggleAxisOn;
    case 2
        toggleAxisOffTight;
    case 3
        toggleAxisOnTight;
    otherwise
        toggleAxisOff;
end

logg.print(1, '\n===================');
logg.print(1, 'Initialize Settings', 'done\n\n');



%% draw components
hold on;
funcRedraw();
funcExport();
hold off;


%% button presses

    function buttonPressFcn(~,evnt)
        %fprintf('key event is: %s\n',evnt.Key);
        if strcmp(evnt.Key,'f1'); logg.print(1, 'print information');	funcPrintInfo(); end
        if strcmp(evnt.Key,'f5'); logg.print(1, 'redraw everything');	funcRedraw(); end
        if strcmp(evnt.Key, 'v'); logg.print(1, 'reset view');          funcResetView(); end
        
        if strcmp(evnt.Key, '1');    toggleVolume(); end
        if strcmp(evnt.Key, '2');     toggleCoils(); end
        if strcmp(evnt.Key, '3');   toggleSensors(); end
        if strcmp(evnt.Key, '4');    toggleFields(); end
        
        if strcmp(evnt.Key, '9');      toggleAxis(); end
        if strcmp(evnt.Key, '0');       toggleROI(); end
        
        if strcmp(evnt.Key, 'g');      toggleGrid(); end
        if strcmp(evnt.Key, 'p');   togglePattern(); end
        if strcmp(evnt.Key, 'c');      toggleClim(); end
        
        if strcmp(evnt.Key, 'x'); logg.print(1, 'cutDir:', 'x'); cutDir = 'x'; end
        if strcmp(evnt.Key, 'y'); logg.print(1, 'cutDir:', 'y'); cutDir = 'y'; end
        if strcmp(evnt.Key, 'z'); logg.print(1, 'cutDir:', 'z'); cutDir = 'z'; end
        
        if strcmp(evnt.Key, 'w')
            funcUpdateCut( 1);
            logg.print(1, 'funcUpdateCut:', '+1');
            if showFields; funcDrawFields(); end
            if showVolume; funcDrawVolume(); end
        end
        if strcmp(evnt.Key, 's')
            funcUpdateCut(-1);
            logg.print(1, 'funcUpdateCut:', '-1');
            if showFields; funcDrawFields(); end
            if showVolume; funcDrawVolume(); end
        end
        if strcmp(evnt.Key, 'a');    toggleCutMode(); end
        if strcmp(evnt.Key, 'd'); toggleCutMovable(); end
        
        if strcmp(evnt.Key,   'uparrow')
            funcUpdateLiveCoils(1);
            funcUpdateLivePlots(1);
            if showPattern
                logg.print(1, 'Live Pattern ( 1):', sprintf('%i', drawPatternID));
            else
                logg.print(1, 'Live Coil ( 1):', sprintf('%i', drawCoilID));
            end
            funcUpdateEffectiveField();
            funcDrawCoils();
            funcDrawFields();
            if ismember(showVolume, [3,4])
                volume = funcUpdateEffectiveFieldVolume;
                funcDrawVolume;
            end
        end
        if strcmp(evnt.Key,   'downarrow')
            funcUpdateLiveCoils(-1);
            funcUpdateLivePlots(-1);
            if showPattern
                logg.print(1, 'Live Pattern ( -1):', sprintf('%i', drawPatternID));
            else
                logg.print(1, 'Live Coil ( -1):', sprintf('%i', drawCoilID));
            end
            funcUpdateEffectiveField();
            funcDrawCoils();
            funcDrawFields();
            if ismember(showVolume, [3,4])
                volume = funcUpdateEffectiveFieldVolume;
                funcDrawVolume;
            end
        end
        if strcmp(evnt.Key,   'rightarrow')
            funcUpdateLiveCoils(10);
            funcUpdateLivePlots(10);
            if showPattern
                logg.print(1, 'Live Pattern (+10):', sprintf('%i', drawPatternID));
            else
                logg.print(1, 'Live Coil (+10):', sprintf('%i', drawCoilID));
            end
            funcUpdateEffectiveField();
            funcDrawCoils();
            funcDrawFields();
            if ismember(showVolume, [3,4])
                volume = funcUpdateEffectiveFieldVolume;
                funcDrawVolume;
            end
        end
        if strcmp(evnt.Key,   'leftarrow')
            funcUpdateLiveCoils(-10);
            funcUpdateLivePlots(-10);
            if showPattern
                logg.print(1, 'Live Pattern (-10):', sprintf('%i', drawPatternID));
            else
                logg.print(1, 'Live Coil (-10):', sprintf('%i', drawCoilID));
            end
            funcUpdateEffectiveField();
            funcDrawCoils();
            funcDrawFields();
            if ismember(showVolume, [3,4])
                volume = funcUpdateEffectiveFieldVolume;
                funcDrawVolume;
            end
        end
        
        if strcmp(evnt.Key, 'i')
            funcUpdateFieldArrowScale(11/10);
            logg.print(1, 'arrow size:', sprintf('%f', fieldArrowScale));
            funcDrawFields();
        end
        if strcmp(evnt.Key, 'j')
            funcUpdateFieldArrowScale(10/11);
            logg.print(1, 'arrow size:', sprintf('%f', fieldArrowScale));
            funcDrawFields();
        end
        if strcmp(evnt.Key, 'n'); toggleNormal(); end
        
        if strcmp(evnt.Key, 'h'); funcPrintHelp(); end
        
    end


%% draw functions
    function funcDrawROI
        if isfield(h, 'roi')
            delete(h.roi);
        end
        switch showROI
            case 1
                h.roi = drawROI(roi, 'roiDrawProperties', roiDrawProperties);
            case 2
                h.roi = drawROI(roi, 'roiDrawProperties', [roiDrawProperties(:)', {'FaceAlpha'}, {0}]);
        end
    end


    function funcDrawCoils
        if isfield(h, 'coils')
            delete(h.coils);
        end
        
        if isfield(setup,'coils')
            
            switch showCoils
                case 1
                    hCoilsLive    = [];
                    if isempty(coilsActive)
                        hCoilsNonLive = [];
                    else
                        hCoilsNonLive = drawCoils(setup.coils(coilsActive), ...
                            'arrowScale', coilArrowScale, ...
                            'coilDrawProperties', coilDrawProperties, ...
                            'coilDrawPropertiesLine', coilDrawPropertiesLine, ...
                            'coilDrawPropertiesArrow', coilDrawPropertiesArrow);
                    end
                case 2
                    if isempty(coilsLive)
                        hCoilsLive = [];
                    else
                        hCoilsLive = drawCoils(setup.coils(coilsLive), ...
                            'arrowScale', coilActiveArrowScale, ...
                            'coilDrawProperties', coilLiveDrawProperties, ...
                            'coilDrawPropertiesLine', coilLiveDrawPropertiesLine, ...
                            'coilDrawPropertiesArrow', coilLiveDrawPropertiesArrow);
                    end
                    if isempty(coilsNotLive)
                        hCoilsNonLive = [];
                    else
                        hCoilsNonLive = drawCoils(setup.coils(coilsNotLive), ...
                            'arrowScale', coilArrowScale, ...
                            'coilDrawProperties', coilDrawProperties, ...
                            'coilDrawPropertiesLine', coilDrawPropertiesLine, ...
                            'coilDrawPropertiesArrow', coilDrawPropertiesArrow);
                    end
                case 3
                    if isempty(coilsLive)
                        hCoilsLive = [];
                    else
                        hCoilsLive = drawCoils(setup.coils(coilsLive), ...
                            'arrowScale', coilActiveArrowScale, ...
                            'coilDrawProperties', coilLiveDrawProperties, ...
                            'coilDrawPropertiesLine', coilLiveDrawPropertiesLine, ...
                            'coilDrawPropertiesArrow', coilLiveDrawPropertiesArrow);
                    end
                    hCoilsNonLive = [];
                otherwise
                    hCoilsLive    = [];
                    hCoilsNonLive = [];
            end
            
            
            h.coils = [hCoilsLive, hCoilsNonLive];
        end
    end


    function funcDrawFields
        if isfield(h, 'fields')
            delete(h.fields);
        end
        if showFields && ~isempty(fields)
            hold on; % hold so that quiver3 (in drawField) does not reset the current view
            h.fields = drawField(effectiveField(fieldCutMap,:), fieldGrid, ...
                'arrowScale', fieldArrowScale, ...
                'normal', fieldsNormal, ...
                'fieldDrawProperties', fieldDrawProperties);
            hold off; % undo hold
        end
    end


    function funcDrawSensors
        if isfield(h, 'sensors')
            delete(h.sensors);
        end
        if showSensors && isfield(setup,'sensors') && ~isempty(sensorsActive)
            h.sensors = drawSensors(setup.sensors(sensorsActive), ...
                'arrowScale', sensorArrowScale, ...
                'sensorDrawProperties', sensorDrawProperties);
        end
    end


    function funcDrawAxis
        axis equal;
        if ismember(showAxis, [1, 3]) % show axis
            axis on;
            if isOctave()
                xlabel(get(fid, 'CurrentAxes'),'x [m]');
                ylabel(get(fid, 'CurrentAxes'),'y [m]');
                zlabel(get(fid, 'CurrentAxes'),'z [m]');
            else % using MATLABs dot notation
                fid.CurrentAxes.XLabel.String = 'x [m]';
                fid.CurrentAxes.YLabel.String = 'y [m]';
                fid.CurrentAxes.ZLabel.String = 'z [m]';
            end
        else
            axis off;
        end
        
        if ismember(showAxis, [2, 3]) % axis tight
            if isOctave()
                xlim(get(fid, 'CurrentAxes'), 'auto')
                ylim(get(fid, 'CurrentAxes'), 'auto')
                zlim(get(fid, 'CurrentAxes'), 'auto')
            else % using MATLABs dot notation
                fid.CurrentAxes.XLimMode = 'auto';
                fid.CurrentAxes.YLimMode = 'auto';
                fid.CurrentAxes.ZLimMode = 'auto';
            end
        else
            if isempty(axisLimits)
                if ~isempty(setup)
                    axisLimits = getSetupBounds(setup, config);
                else
                    axisLimits = roi;
                end
            end
            
            if isOctave()
                xlim(get(fid, 'CurrentAxes'), axisLimits.x)
                ylim(get(fid, 'CurrentAxes'), axisLimits.y)
                zlim(get(fid, 'CurrentAxes'), axisLimits.z)
            else % using MATLABs dot notation
                fid.CurrentAxes.XLim = axisLimits.x;
                fid.CurrentAxes.YLim = axisLimits.y;
                if is3D
                    fid.CurrentAxes.ZLim = axisLimits.z;
                end
            end
        end
        
    end

    function funcDrawVolume
        if isfield(h, 'volume')
            delete(h.volume);
        end
        if showVolume
            if showVolume==1
                h.volume = drawVolume([], res, roi, 'clim', clim, 'cuts', cuts, 'showVolume', false, 'showGrid', true);
            else
                h.volume = drawVolume(volume, res, roi, 'clim', clim, 'cuts', cuts, 'showGrid', showVolumeGrid);
            end
        end
    end


    function funcDrawAxisRegion
        if isfield(h, 'axisRegion')
            delete(h.axisRegion);
        end
        if showAxisRegion
            h.axisRegion = drawROI(axisLimits, 'roiDrawProperties', axisRegionDrawProperties);
        end
    end


%% update function

    function fieldVolume = funcUpdateEffectiveFieldVolume
        if showVolume==4
            fieldVolume = reshape(log10(sqrt(sum(effectiveField.^2,2))), res);
        else
            fieldVolume = reshape(sqrt(sum(effectiveField.^2,2)), res);
        end
    end


%     function funcUpdateFigureTitle
%         %set(fid, 'Name', sprintf('Cut [%s, %i]: (x: [%i,%i], y: [%i,%i], z: [%i,%i])', cutMode, cutMovable, cuts.x(1),cuts.x(2),cuts.y(1),cuts.y(2),cuts.z(1),cuts.z(2)));
%     end


    function funcUpdateEffectiveField % update the current effectiveField
        if ~isempty(fields)
            if showFields || ismember(showVolume, [3,4])
                if showPattern
                    effectiveField = 0;
                    for j = 1:numOfCoils
                        effectiveField = effectiveField + fields{j} .* config.currentPattern(drawPatternID,j);
                    end
                else
                    effectiveField = fields{drawCoilID};
                end
            end
        end
    end

    function funcUpdateFieldArrowScale(val)
        fieldArrowScale = fieldArrowScale*val;
    end


    function out = is3D
        if isempty(setup)
            if ~isempty(volumeCustom)
                out = ndims(volumeCustom) == 3;
            else
                out = 0;
            end
        else
            out = setup.dim == 3;
        end
    end

    function funcUpdateCut(val)
        switch cutDir
            case 'x'; i = 1;
            case 'y'; i = 2;
            case 'z'; i = 3;
        end
        switch cutMode
            case 'Min'; j = 1;
            case 'Max'; j = 2;
            otherwise; j = 3;
        end
        if j==3
            newCut 	= cuts.(cutDir)(:) + val;
            if cutMovable || ~(newCut(1) < 1 || newCut(2) > res(i))
                cuts.(cutDir)(:)      = max(1, min(newCut, res(i)));
            end
        else
            newCut	= cuts.(cutDir)(j) + val;
            if cutMovable
                newCut = max(1, min(newCut, res(i)));
                if (j==1 && cuts.(cutDir)(3-j) < newCut); cuts.(cutDir)(3-j) = newCut; end
                if (j==2 && cuts.(cutDir)(3-j) > newCut); cuts.(cutDir)(3-j) = newCut; end
            else
                if (j==1); newCut = max(1, min(newCut, cuts.(cutDir)(3-j))); end
                if (j==2); newCut = max(cuts.(cutDir)(3-j), min(newCut, res(i))); end
            end
            cuts.(cutDir)(j) = newCut;
        end
        [fieldGrid, fieldCutMap] = createVoxelGrid(roi, res, 'b', cuts);
    end

    function funcUpdateLivePlots(val)
        if showPattern
            logg.print(1, sprintf('Live Pattern (%i):', val), sprintf('%i', drawPatternID));
        else
            logg.print(1, sprintf('Live Coil (%i):', val), sprintf('%i', drawCoilID));
        end
        funcUpdateEffectiveField();
        funcDrawCoils();
        funcDrawFields();
        if ismember(showVolume, [3,4])
            volume = funcUpdateEffectiveFieldVolume;
            funcDrawVolume;
        end
    end


    function sOut = funcUpdateLiveCoils(val)
        if isempty(coilsActive)
            coilsLive       = [];
            coilsNotLive    = [];
            sOut = sprintf('no coils available');
        elseif ~isempty(config)
            if showPattern
                coilsLive   = [];
                while isempty(coilsLive)
                    drawPatternID   = drawPatternID + val;
                    drawPatternID   = mod(drawPatternID-1, numOfPatterns)+1;
                    if ~any(config.currentPattern(drawPatternID,:))
                        continue;
                    end
                    coilsLive       = config.coilsActive(config.currentPattern(drawPatternID,:)>0);
                    coilsNotLive    = config.coilsActive;
                    coilsNotLive(ismember(coilsNotLive,coilsLive)) = [];
                    sOut = sprintf('draw PatternID: %03i', drawPatternID);
                end
            else
                drawCoilID      = drawCoilID + val;
                drawCoilID      = mod(drawCoilID-1, numOfCoils)+1;
                coilsLive       = config.coilsActive(drawCoilID);
                coilsNotLive    = config.coilsActive;
                coilsNotLive(ismember(coilsNotLive,coilsLive)) = [];
                sOut = sprintf('draw CoilID: %03i', drawCoilID);
            end
        else
            drawCoilID      = drawCoilID + val;
            drawCoilID      = mod(drawCoilID-1, numOfCoils)+1;
            coilsLive       = coilsActive(drawCoilID);
            coilsNotLive    = coilsActive;
            coilsNotLive(ismember(coilsNotLive,coilsLive)) = [];
            sOut = sprintf('draw CoilID: %03i', drawCoilID);
        end
    end


%% toggle functions

    function toggleAxisOff
        toggleAxis  = @toggleAxisOnTight;
        showAxis    = 0;
        logg.print(1, 'Axis:', 'off');
        funcDrawAxis;
    end
    function toggleAxisOn
        toggleAxis  = @toggleAxisOff;
        showAxis    = 1;
        logg.print(1, 'Axis:', 'on');
        funcDrawAxis;
    end
    function toggleAxisOffTight
        toggleAxis  = @toggleAxisOn;
        showAxis    = 2;
        logg.print(1, 'Axis:', 'off - tight');
        funcDrawAxis;
    end
    function toggleAxisOnTight
        toggleAxis  = @toggleAxisOffTight;
        showAxis    = 3;
        logg.print(1, 'Axis:', 'on - tight');
        funcDrawAxis;
    end

    function toggleROIOff
        toggleROI   = @toggleROIOn;
        showROI     = 0;
        logg.print(1, 'ROI:', 'off');
        funcDrawROI;
    end
    function toggleROIOn
        toggleROI   = @toggleROIGrid;
        showROI     = 1;
        logg.print(1, 'ROI:', 'on');
        funcDrawROI;
    end
    function toggleROIGrid
        toggleROI   = @toggleROIOff;
        showROI     = 2;
        logg.print(1, 'ROI:', 'grid');
        funcDrawROI;
    end

    function toggleSensorsOff
        toggleSensors = @toggleSensorsOn;
        showSensors = false;
        logg.print(1, 'Sensors:', 'off');
        funcDrawSensors;
    end
    function toggleSensorsOn
        toggleSensors = @toggleSensorsOff;
        showSensors = true;
        logg.print(1, 'Sensors:', 'on');
        funcDrawSensors;
    end

    function toggleCoilsOff
        toggleCoils = @toggleCoilsOn;
        showCoils = 0;
        logg.print(1, 'Coils:', 'off');
        funcDrawCoils;
    end
    function toggleCoilsOn
        toggleCoils = @toggleCoilsLive;
        showCoils = 1;
        logg.print(1, 'Coils:', 'on');
        funcDrawCoils;
    end
    function toggleCoilsLive
        toggleCoils = @toggleCoilsLiveOnly;
        showCoils = 2;
        logg.print(1, 'Coils:', 'live');
        funcDrawCoils;
    end
    function toggleCoilsLiveOnly
        toggleCoils = @toggleCoilsOff;
        showCoils = 3;
        logg.print(1, 'Coils:', 'live only');
        funcDrawCoils;
    end

    function toggleVolumeOff
        toggleVolume = @toggleVolumeGrid;
        showVolume = 0;
        logg.print(1, 'Volume:', 'off');
        funcDrawVolume;
    end
    function toggleVolumeGrid
        toggleVolume = @toggleVolumeOn;
        showVolume = 1;
        logg.print(1, 'Volume:', 'grid');
        funcDrawVolume;
    end
    function toggleVolumeOn
        if isempty(volumeCustom)
            logg.print(0, 'Volume:', 'volume (skipped: no volume given)');
            toggleVolumeField;
            return;
        end
        toggleVolume = @toggleVolumeField;
        volume = volumeCustom;
        showVolume = 2;
        logg.print(1, 'Volume:', 'volume');
        if showROI
            toggleROIGrid;
        end
        funcDrawVolume;
    end
    function toggleVolumeField
        if isempty(fields)
            logg.print(0, 'Volume:', 'field (skipped: no field given)');
            toggleVolumeOff;
            return;
        end
        toggleVolume = @toggleVolumeFieldLog;
        showVolume = 3;
        funcUpdateEffectiveField();
        volume = funcUpdateEffectiveFieldVolume;
        logg.print(1, 'Volume:', 'field');
        toggleFieldsOff;
        if showROI
            toggleROIGrid;
        end
        funcDrawVolume;
    end
    function toggleVolumeFieldLog
        if isempty(fields)
            logg.print(0, 'Volume:', 'field (skipped: no field given)');
            toggleVolumeOff;
            return;
        end
        toggleVolume = @toggleVolumeOff;
        showVolume = 4;
        funcUpdateEffectiveField();
        volume = funcUpdateEffectiveFieldVolume;
        logg.print(1, 'Volume:', 'log10(field)');
        toggleFieldsOff;
        if showROI
            toggleROIGrid;
        end
        funcDrawVolume;
    end


    function toggleGridOff
        toggleGrid = @toggleGridOn;
        showVolumeGrid = false;
        logg.print(1, 'Grid:', 'off');
        funcDrawVolume;
    end
    function toggleGridOn
        toggleGrid = @toggleGridOff;
        showVolumeGrid = true;
        logg.print(1, 'Grid:', 'on');
        funcDrawVolume;
    end

    function togglePatternOff
        togglePattern = @togglePatternOn;
        showPattern = false;
        logg.print(1, 'Pattern:', 'off');
        funcUpdateLiveCoils(0);
        funcUpdateLivePlots(0);
    end

    function togglePatternOn
        if isempty(config)
            logg.print(0, 'Pattern:', 'on (skipped: no config given)');
            togglePatternOff;
            return;
        end
        if ~any(config.currentPattern(:))
            logg.print(0, 'Pattern:', 'on (skipped: no pattern available that uses the available subset of coils)');
            togglePatternOff;
            return;
        end
        togglePattern = @togglePatternOff;
        showPattern = true;
        logg.print(1, 'Pattern:', 'on');
        funcUpdateLiveCoils(0);
        funcUpdateLivePlots(0);
    end

    function toggleFieldsOff
        toggleFields = @toggleFieldsOn;
        showFields = false;
        logg.print(1, 'Fields:', 'off');
        funcDrawFields;
    end
    function toggleFieldsOn
        if isempty(fields)
            logg.print(0, 'Fields:', 'on (skipped: no fields given)');
            toggleFieldsOff;
            return;
        end
        toggleFields = @toggleFieldsOff;
        showFields = true;
        funcUpdateEffectiveField();
        logg.print(1, 'Fields:', 'on');
        funcDrawFields;
    end
    function toggleNormalOn
        toggleNormal = @toggleNormalOff;
        fieldsNormal = true;
        logg.print(1, 'fieldsNormal:', 'on');
        funcDrawFields;
    end
    function toggleNormalOff
        toggleNormal = @toggleNormalOn;
        fieldsNormal = false;
        logg.print(1, 'fieldsNormal:', 'off');
        funcDrawFields;
    end

    function toggleClimOn
        if isempty(climCustom)
            logg.print(0, 'clim:', 'custom (skipped: no custom clim given)');
            toggleClimOff;
            return;
        end
        toggleClim = @toggleClimOff;
        clim = climCustom;
        logg.print(1, 'clim:', 'custom');
        funcDrawVolume;
    end
    function toggleClimOff
        toggleClim = @toggleClimOn;
        clim = [];
        logg.print(1, 'clim:', 'auto');
        funcDrawVolume;
    end

    function toggleCutModeMax
        toggleCutMode   = @toggleCutModeMin;
        cutMode         = 'Max';
        logg.print(1, 'CutMode:', cutMode);
    end

    function toggleCutModeMin
        toggleCutMode   = @toggleCutModeBoth;
        cutMode         = 'Min';
        logg.print(1, 'CutMode:', cutMode);
    end

    function toggleCutModeBoth
        toggleCutMode   = @toggleCutModeMax;
        cutMode         = 'Both';
        logg.print(1, 'CutMode:', cutMode);
    end

    function toggleCutMovableTrue
        toggleCutMovable    = @toggleCutMovableFalse;
        cutMovable          = true;
        logg.print(1, 'CutMovable:', 'on');
    end
    function toggleCutMovableFalse
        toggleCutMovable    = @toggleCutMovableTrue;
        cutMovable          = false;
        logg.print(1, 'CutMovable:', 'off');
    end



%% other functions

    function funcPrintInfo
        fpon;
        fpon('To see the help/documentation press ''h''.')
        fpon;
        fpon('Options:');
        
        %         [num2cell(1:numel(p.Parameters))',p.Parameters']
        %
        %         {[ 1]}    {'clim'                       }
        %         {[ 2]}    {'coilActiveArrowScale'       }
        %         {[ 3]}    {'coilArrowScale'             }
        %         {[ 4]}    {'coilDrawProperties'         }
        %         {[ 5]}    {'coilDrawPropertiesArrow'    }
        %         {[ 6]}    {'coilDrawPropertiesLine'     }
        %         {[ 7]}    {'coilLiveDrawProperties'     }
        %         {[ 8]}    {'coilLiveDrawPropertiesArrow'}
        %         {[ 9]}    {'coilLiveDrawPropertiesLine' }
        %         {[10]}    {'coilsActive'                }
        %         {[11]}    {'config'                     }
        %         {[12]}    {'cuts'                       }
        %         {[13]}    {'drawCoilID'                 }
        %         {[14]}    {'drawPatternID'              }
        %         {[15]}    {'export'                     }
        %         {[16]}    {'exportFormat'               }
        %         {[17]}    {'exportRenderer'             }
        %         {[18]}    {'fid'                        }
        %         {[19]}    {'fieldArrowScale'            }
        %         {[20]}    {'fieldDrawProperties'        }
        %         {[21]}    {'fields'                     }
        %         {[22]}    {'fieldsNormal'               }
        %         {[23]}    {'res'                        }
        %         {[24]}    {'roi'                        }
        %         {[25]}    {'roiDrawProperties'          }
        %         {[26]}    {'sensorArrowScale'           }
        %         {[27]}    {'sensorDrawProperties'       }
        %         {[28]}    {'sensorsActive'              }
        %         {[29]}    {'setup'                      }
        %         {[30]}    {'showAxis'                   }
        %         {[31]}    {'showCoils'                  }
        %         {[32]}    {'showFields'                 }
        %         {[33]}    {'showPattern'                }
        %         {[34]}    {'showROI'                    }
        %         {[35]}    {'showSensors'                }
        %         {[36]}    {'showVolume'                 }
        %         {[37]}    {'showVolumeGrid'             }
        %         {[38]}    {'verbose'                    }
        %         {[39]}    {'view'                       }
        %         {[40]}    {'volume'                     }
        
        % TODO: implement information so that the present visualization can
        % be recreated gracefully.
        
        %if changedParameter(12)
        fprintf('''cuts'', struct(''x'',[%i, %i],''y'',[%i, %i],''z'',[%i, %i])\n', cuts.x(1),cuts.x(2),cuts.y(1),cuts.y(2),cuts.z(1),cuts.z(2));
        %end
        
        
    end


    function funcPrintHelp
        fpon;
        fpon('HELP/DOCUMENTATION')
        fpon;
        fpon('HOTKEYS')
        fpon('h        show this help');
        fpon('f1       print information');
        fpon('f5       redraw figure');
        fpon;
        fpon('v        reset view');
        fpon;
        fpon('1        toggles parameter ''showVolume''');
        fpon('2        toggles parameter ''showCoils''');
        fpon('3        toggles parameter ''showSensors''');
        fpon('4        toggles parameter ''showFields''');
        fpon('9        toggles parameter ''showAxis''');
        fpon('0        toggles parameter ''showROI''');
        fpon;
        fpon('g        toggles parameter ''showVolumeGrid''');
        fpon('p        toggles parameter ''showPattern''');
        fpon('c        toggles usage of provided clim');
        fpon;
        fpon('x        set cut direction');
        fpon('y        set cut direction');
        fpon('z        set cut direction');
        fpon('a        toggle cut side');
        fpon('d        toggle pushable cut edge');
        fpon('w        increase cut position');
        fpon('s        decrease cut position');
        fpon;
        fpon('up       display next coil/pattern field');
        fpon('down     display prev coil/pattern field');
        fpon('right    display next coil/pattern field (x10)');
        fpon('left     display prev coil/pattern field (x10)');
        fpon('i        increase field arrow size');
        fpon('j        decrease field arrow size');
        fpon('n        toggle show normalized field');
        
        
    end


    function funcRedraw
        cla;
        fid.CurrentAxes.XDir = 'normal';
        fid.CurrentAxes.YDir = 'normal';
        funcDrawVolume();
        funcDrawROI();
        funcDrawCoils();
        funcDrawSensors();
        funcDrawFields();
        funcDrawAxis();
        funcDrawAxisRegion();
        funcResetView();
    end

    function funcResetView
        if ~isempty(customView)
            view(customView(:)');
        else
            if is3D
                view(-40,20);
            else
                view(0,90);
            end
        end
    end

    function funcExport
        if ~isempty(exportFileName)
            if strcmp(exportFileName(end), '/')
                fpop('Export failed:','No filename specified');
            else
                switch exportFileFormat
                    case {'PNG', 'png', '-dpng'}
                        fileFormat = '-dpng';
                        fileEnding = '.png';
                    case {'JPG', 'jpg', 'JPEG', 'jpeg', '-djpeg'}
                        fileFormat = '-djpeg';
                        fileEnding = '.jpeg';
                    case {'SVG', 'svg', '-dsvg'}
                        fileFormat = '-dsvg';
                        fileEnding = '.svg';
                    otherwise
                        fileFormat = '-dpdf';
                        fileEnding = '.pdf';
                end
                fpop('Export visualization:',[exportFileName, fileEnding]);
                
                if exist([exportFileName, fileEnding], 'file') && ~exportOverride
                    fpop('Export visualization:','skipped! (file already exist)');
                else
                    
                    curU    = get(fid, 'Units');
                    set(fid, 'Units', 'centimeters');
                    curPPM  = get(fid, 'PaperPositionMode');
                    set(fid, 'PaperPositionMode', 'Auto');
                    curPU   = get(fid, 'PaperUnits');
                    set(fid, 'PaperUnits', 'centimeters');
                    pos     = get(fid, 'Position');
                    curPS   = get(fid, 'PaperSize');
                    set(fid, 'PaperSize', [pos(3), pos(4)]);
                    
                    if exportRenderer
                        fpop('Export visualization:','''painters''-Renderer active: this may take some time...');
                        curRenderer = get(fid, 'Renderer');
                        set(fid, 'Renderer', 'painters');
                    end
                    
                    
                    warning('off', 'MATLAB:print:FigureTooLargeForPage');
                    print(fid, [exportFileName, fileEnding], fileFormat);
                    
                    if exportRenderer
                        set(fid, 'Renderer', curRenderer);
                    end
                    
                    set(fid, 'Units', curU);
                    set(fid, 'PaperPositionMode', curPPM);
                    set(fid, 'PaperUnits', curPU);
                    set(fid, 'PaperSize', curPS);
                    
                    fpop('Export visualization:','done!');
                end
            end
        end
    end

%% example
    function [h, fid] = runExample()
        fid         = gcf;
        cla;
        
        [setup, config] = loadExampleMRXDataset(3);
        
        if setup.dim == 3
            %setup.coils = rmfield(setup.coils,'Segments');
            
            
            res = [10,5,3];
            volume = zeros(res);
            volume(  1,  1,  1) = 0.1;
            volume(end,  1,  1) = 0.2;
            volume(  1,end,  1) = 0.3;
            volume(  1,  1,end) = 0.4;
            volume(  1,end,end) = 0.5;
            volume(end,  1,end) = 0.6;
            volume(end,end,  1) = 0.7;
            volume(end,end,end)	= 0.8;
        else
            res = [10,10,1];
            volume = createPhantom([10,10,1], 'P', 'options', struct('XMNP',100));
        end
        
        fieldGrid   = createVoxelGrid(setup.roi, res);
        magnFields  = createExcitationFields(setup.coils, fieldGrid);
        
        [h, fid] = visualizeMRX(setup, ...
            'config',           config, ...
            'volume',           volume, ...
            'showROI',          2, ...
            'showSensors',      0, ...
            'showCoils',        3, ...
            'showFields',       1, ...
            'showVolume',       0, ...
            'fieldArrowScale',  0.009, ...
            'fields',           magnFields, ...
            'fid',              fid);
    end


end

