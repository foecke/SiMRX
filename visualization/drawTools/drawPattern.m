%DRAWPATTERN visualizes a magnetic field induced by a coil pattern
%
%   h = DRAWPATTERN(magnFields, currentPattern, voxelGrid) visualizes the
%   magnetic fields that corresponds to a currentPattern on the given
%   voxelgrid. The returned h is the respective function handle.
%
%   h = DRAWPATTERN(..., varargin) allows
%   optional settings/parameter:
%       'arrowScale'     integer to scale the arrow size used by quiver.
%                       Default: 1 (unscaled)
%       'normal'        true or false, allows to normalize the vector
%                       fields in field. Default: false.
%       'shift'         shifts the object by given vector
%                       (default: [0,0,0])
%       'fieldDrawProperties' Cell of Line Properties that is forwarded to the
%                       respective calls


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function h = drawPattern(magnFields, currentPattern, voxelGrid, varargin)

currentField = 0;
for j = 1:numel(magnFields)
    currentField = currentField + magnFields{j} .* currentPattern(j);
end

h.fields = drawField(currentField, voxelGrid, varargin{:});

end

