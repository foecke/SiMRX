%DRAWFIELD visualizes the magnetic field induced by coils
%
%   h = DRAWFIELD(field, voxelGrid) visualizes the magnetic fields in field
%   on the given voxelgrid. The returned h is the respective function
%   handle.
%
%   h = DRAWFIELD(..., varargin) allows optional
%   settings/parameter:
%       'arrowScale'     integer to scale the arrow size used by quiver.
%                       Default: 1 (unscaled)
%       'normal'        true or false, allows to normalize the vector
%                       fields in field. Default: false.
%       'shift'         shifts the object by given vector
%                       (default: [0,0,0])
%       'fieldDrawProperties' Cell of Line Properties that is forwarded to the
%                       respective calls


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function h = drawField(field, voxelGrid, varargin)

presets = loadVisPresets();

%% parse inputs
p = inputParser;
p.addParameter('arrowScale'         , presets.fieldArrowScale   , @isnumeric);
p.addParameter('normal'         	, 0                         , @(s) ismember(s, 0:1));
p.addParameter('shift'              , [0,0,0]                   , @(s) (isnumeric(s) && numel(s)==3));
p.addParameter('fieldDrawProperties', presets.fieldDrawProperties);

p.parse(varargin{:});

arrowScale              = p.Results.arrowScale;
normal                  = p.Results.normal;
shift                   = p.Results.shift;
fieldDrawProperties 	= p.Results.fieldDrawProperties;


%% parse data
if normal
    field = field ./ repmat(vecnorm(field, 2, 2),[1,size(field,2)]);
end
field = arrowScale.* field;


%% function
%     if nnz(voxelGrid)) == 2
%         h =  quiver(voxelGrid(:,1)+shift(1), voxelGrid(:,2)+shift(2), ...
%             field(:,1), field(:,2), ...
%             0, fieldDrawProperties{:});
%     else
h = quiver3(voxelGrid(:,1)+shift(1), voxelGrid(:,2)+shift(2), voxelGrid(:,3)+shift(3), ...
    field(:,1), field(:,2), field(:,3), ...
    0, fieldDrawProperties{:});
%     end


end

