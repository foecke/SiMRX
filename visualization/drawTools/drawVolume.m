%DRAWVOLUME visualizes volume data.
%
%   h = DRAWVOLUME(volume, res, roi) visualizes sensors. roi is a set of
%   intervals that define the region of interest. The returned h is the
%   respective function handle.
%
%   h = drawVolume(..., varargin) allows optional
%   settings/parameter:
%       'cuts'          make optional cuts in the volume.
%                       Default: size(volume)
%       'clim'          color limits used for plot
%
%   As of now, only cuts in z direction are supported!


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       25-May-2021


function h = drawVolume(volume, res, roi, varargin)

%% parse inputs
p = inputParser;
p.addParameter('cuts',          [], 	@isstruct);
p.addParameter('clim',          [],   	@(s) (isnumeric(s) && numel(s)==2) || isempty(s));
p.addParameter('showVolume',	true, 	@(s) ismember(s, 0:1));
p.addParameter('showGrid',      true,  	@(s) ismember(s, 0:1));

p.parse(varargin{:});

cuts            = p.Results.cuts;
clim            = p.Results.clim;
showVolume      = p.Results.showVolume;
showGrid 	    = p.Results.showGrid;

if isempty(res)
    if ~isempty(volume)
        res = size(volume);
    else
        res = [1,1,1];
    end
else
    if ~isempty(volume)
        volume = reshape(volume, res);
    end
end

if range(roi.z) == 0 % check if 2D or 3D
    voxelgridRes    = [res([1,2])+1,1];
else
    voxelgridRes    = res+1;
end

if isempty(cuts)
    cuts.x = [1, res(1)];
    cuts.y = [1, res(2)];
    cuts.z = [1, res(3)];
end


%%
curFaceColor        = 'none';
if showVolume
    curFaceColor    = 'texturemap';
    if isempty(volume)
        clim = [0,1];
    end
    if isempty(clim)
        clim = [min(volume(:)), max(volume(:))];
    end
    if clim(1) == clim(2)
        clim(2) = clim(1)+1;
    end
end
curEdgeColor        = 'none';
if showGrid
    curEdgeColor	= 'black';
end

vg                  = createVoxelGrid(roi, voxelgridRes,'-');

vgx                 = unique(vg(:,1));
vgy                 = unique(vg(:,2));
vgz                 = unique(vg(:,3));
[Y,X,Z]             = meshgrid(vgy, vgx, vgz);


%%
if range(roi.z) == 0 % check if 2D or 3D
    if isempty(volume)
        curCData = [];
    else
        curCData   = squeeze(volume(cuts.x(1):cuts.x(2),cuts.y(1):cuts.y(2)));
    end
    h = surface(...
        squeeze(X(cuts.x(1):cuts.x(2)+1,cuts.y(1):cuts.y(2)+1)),...
        squeeze(Y(cuts.x(1):cuts.x(2)+1,cuts.y(1):cuts.y(2)+1)),...
        squeeze(Z(cuts.x(1):cuts.x(2)+1,cuts.y(1):cuts.y(2)+1)),...
        'CData', curCData, ...
        'FaceColor', curFaceColor, ...
        'CDataMapping', 'scaled', ...
        'EdgeColor', curEdgeColor);
    if ~isempty(volume)
        set(get(h, 'Parent'), 'CLim', clim);
    end
else
    
    % left face
    if isempty(volume)
        curCData = [];
    else
        currentVolume   = squeeze(volume(cuts.x(1),cuts.y(1):cuts.y(2),cuts.z(1):cuts.z(2)));
        if (cuts.z(1)==cuts.z(2))
            curCData    = currentVolume';
        elseif (cuts.y(1)==cuts.y(2))
            curCData    = currentVolume';
        else
            curCData    = currentVolume;
        end
    end
    hymin           = surface(...
        squeeze(X(cuts.x(1),cuts.y(1):cuts.y(2)+1,cuts.z(1):cuts.z(2)+1)),...
        squeeze(Y(cuts.x(1),cuts.y(1):cuts.y(2)+1,cuts.z(1):cuts.z(2)+1)),...
        squeeze(Z(cuts.x(1),cuts.y(1):cuts.y(2)+1,cuts.z(1):cuts.z(2)+1)),...
        'CData', curCData, ...
        'FaceColor', curFaceColor, ...
        'CDataMapping', 'scaled', ...
        'EdgeColor', curEdgeColor);
    if ~isempty(volume)
        set(get(hymin, 'Parent'), 'CLim', clim);
    end
    
    
    % right face
    if isempty(volume)
        curCData = [];
    else
        currentVolume   = squeeze(volume(cuts.x(2),cuts.y(1):cuts.y(2),cuts.z(1):cuts.z(2)));
        if (cuts.y(1)==cuts.y(2))
            curCData    = currentVolume';
        elseif (cuts.z(1)==cuts.z(2))
            curCData    = currentVolume';
        else
            curCData    = currentVolume;
        end
    end
    hymax           = surface(...
        squeeze(X(cuts.x(2)+1,cuts.y(1):cuts.y(2)+1,cuts.z(1):cuts.z(2)+1)),...
        squeeze(Y(cuts.x(2)+1,cuts.y(1):cuts.y(2)+1,cuts.z(1):cuts.z(2)+1)),...
        squeeze(Z(cuts.x(2)+1,cuts.y(1):cuts.y(2)+1,cuts.z(1):cuts.z(2)+1)),...
        'CData',    curCData, ...
        'FaceColor', curFaceColor, ...
        'CDataMapping', 'scaled', ...
        'EdgeColor', curEdgeColor);
    if ~isempty(volume)
        set(get(hymax, 'Parent'), 'CLim', clim);
    end
    
    
    % front face
    if isempty(volume)
        curCData = [];
    else
        currentVolume   = squeeze(volume(cuts.x(1):cuts.x(2),cuts.y(1),cuts.z(1):cuts.z(2)));
        if (cuts.x(1)==cuts.x(2))
            curCData    = currentVolume';
        else
            curCData    = currentVolume;
        end
    end
    hxmin           = surface(...
        squeeze(X(cuts.x(1):cuts.x(2)+1,cuts.y(1),cuts.z(1):cuts.z(2)+1)),...
        squeeze(Y(cuts.x(1):cuts.x(2)+1,cuts.y(1),cuts.z(1):cuts.z(2)+1)),...
        squeeze(Z(cuts.x(1):cuts.x(2)+1,cuts.y(1),cuts.z(1):cuts.z(2)+1)),...
        'CData', curCData, ...
        'FaceColor', curFaceColor, ...
        'CDataMapping', 'scaled', ...
        'EdgeColor', curEdgeColor);
    if ~isempty(volume)
        set(get(hxmin, 'Parent'), 'CLim', clim);
    end
    
    
    % back face
    if isempty(volume)
        curCData = [];
    else
        currentVolume   = squeeze(volume(cuts.x(1):cuts.x(2),cuts.y(2),cuts.z(1):cuts.z(2)));
        if (cuts.x(1)==cuts.x(2))
            curCData    = currentVolume';
        else
            curCData    = currentVolume;
        end
    end
    hxmax           = surface(...
        squeeze(X(cuts.x(1):cuts.x(2)+1,cuts.y(2)+1,cuts.z(1):cuts.z(2)+1)),...
        squeeze(Y(cuts.x(1):cuts.x(2)+1,cuts.y(2)+1,cuts.z(1):cuts.z(2)+1)),...
        squeeze(Z(cuts.x(1):cuts.x(2)+1,cuts.y(2)+1,cuts.z(1):cuts.z(2)+1)),...
        'CData', curCData, ...
        'FaceColor', curFaceColor, ...
        'CDataMapping', 'scaled', ...
        'EdgeColor', curEdgeColor);
    if ~isempty(volume)
        set(get(hxmax, 'Parent'), 'CLim', clim);
    end
    
    
    % bottom face
    if isempty(volume)
        curCData = [];
    else
        currentVolume   = squeeze(volume(cuts.x(1):cuts.x(2),cuts.y(1):cuts.y(2),cuts.z(1)));
        curCData        = currentVolume;
    end
    hzmin           = surface(...
        squeeze(X(cuts.x(1):cuts.x(2)+1,cuts.y(1):cuts.y(2)+1,cuts.z(1))),...
        squeeze(Y(cuts.x(1):cuts.x(2)+1,cuts.y(1):cuts.y(2)+1,cuts.z(1))),...
        squeeze(Z(cuts.x(1):cuts.x(2)+1,cuts.y(1):cuts.y(2)+1,cuts.z(1))),...
        'CData', curCData, ...
        'FaceColor', curFaceColor, ...
        'CDataMapping', 'scaled', ...
        'EdgeColor', curEdgeColor);
    if ~isempty(volume)
        set(get(hzmin, 'Parent'), 'CLim', clim);
    end
    
    
    % top face
    if isempty(volume)
        curCData = [];
    else
        currentVolume   = squeeze(volume(cuts.x(1):cuts.x(2),cuts.y(1):cuts.y(2),cuts.z(2)));
        curCData        = currentVolume;
    end
    hzmax           = surface(...
        squeeze(X(cuts.x(1):cuts.x(2)+1,cuts.y(1):cuts.y(2)+1,cuts.z(2)+1)),...
        squeeze(Y(cuts.x(1):cuts.x(2)+1,cuts.y(1):cuts.y(2)+1,cuts.z(2)+1)),...
        squeeze(Z(cuts.x(1):cuts.x(2)+1,cuts.y(1):cuts.y(2)+1,cuts.z(2)+1)),...
        'CData', curCData, ...
        'FaceColor', curFaceColor, ...
        'CDataMapping', 'scaled', ...
        'EdgeColor', curEdgeColor);
    if ~isempty(volume)
        set(get(hzmax, 'Parent'), 'CLim', clim);
    end
    
    % collect all surface handles
    h = [hymin, hymax, hxmin, hxmax, hzmin, hzmax];
    
    
end


end

