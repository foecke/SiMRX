%DRAWSENSORS visualizes sensors
%
%   h = DRAWSENSORS(sensors) visualizes sensors. The returned h is the
%   respective function handle.
%
%   h = DRAWSENSORS(..., varargin) allows optional
%   settings/parameter:
%       'arrowScale'     integer to scale the arrow size used by quiver.
%                       Default: 1 (unscaled)
%       'sensorDrawProperties' Cell of Line Properties that is forwarded to the
%                       respective calls
%       'shift'         shifts the object by given vector
%                       (default: [0,0,0])


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function h = drawSensors(sensors, varargin)

presets = loadVisPresets();

%% parse inputs
p = inputParser;
p.addParameter('arrowScale', presets.sensorArrowScale, @isnumeric);
p.addParameter('shift', [0,0,0], @(s) (isnumeric(s) && numel(s)==3));

p.addParameter('sensorDrawProperties', presets.sensorDrawProperties);

p.parse(varargin{:});

arrowScale               = p.Results.arrowScale;
shift                   = p.Results.shift;
sensorDrawProperties    = p.Results.sensorDrawProperties;


%% function
ax = gca;
holdStatus  = ishold(ax);
hold on;

positions   = vertcat(sensors.Position);
normals     = arrowScale .* vertcat(sensors.Normal) ./ vecnorm(vertcat(sensors.Normal),2,2);

h = quiver3(positions(:,1)+shift(1),positions(:,2)+shift(2),positions(:,3)+shift(3),normals(:,1),normals(:,2),normals(:,3),0,sensorDrawProperties{:});

if ~holdStatus; hold off; end


end

