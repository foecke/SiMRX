%DRAWCOILS visualizes coils
%
%   h = DRAWCOILS(coils) visualizes coils. The returned h is the respective
%   function handle.
%
%   h = DRAWCOILS(..., varargin) allows optional
%   settings/parameter:
%       'arrowScale'    integer to scale the arrow size used by quiver.
%       'shift'         shifts the object by given vector
%                       (default: [0,0,0])
%       'coilDrawProperties' properties to draw the coils.
%                       See documentation for 'Line Properties'
%       'coilDrawPropertiesLine' line properties which are applied in the
%                       coil loop case.
%                       See documentation for 'Line Properties'
%       'coilDrawPropertiesArrow' line properties which are applied in the
%                       simple coil case.
%                       See documentation for 'Quiver Properties'


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function h = drawCoils(coils, varargin)

presets = loadVisPresets();
%% parse inputs
p = inputParser;
p.addParameter('arrowScale', presets.coilArrowScale, @isnumeric);
p.addParameter('shift', [0,0,0], @(s) (isnumeric(s) && numel(s)==3));

p.addParameter('coilDrawProperties',        presets.coilDrawProperties);
p.addParameter('coilDrawPropertiesLine',    presets.coilDrawPropertiesLine);
p.addParameter('coilDrawPropertiesArrow', 	presets.coilDrawPropertiesArrow);

p.parse(varargin{:});

arrowScale              = p.Results.arrowScale;
shift                   = p.Results.shift;
coilDrawProperties      = p.Results.coilDrawProperties;
coilDrawPropertiesLine  = p.Results.coilDrawPropertiesLine;
coilDrawPropertiesArrow = p.Results.coilDrawPropertiesArrow;


%% function
ax = gca;
holdStatus  = ishold(ax);
hold on;

if isfield(coils, 'Segments')
    for i = 1:numel(coils)
        h(i) = plot3(coils(i).Segments(:,1)+shift(1),coils(i).Segments(:,2)+shift(2),coils(i).Segments(:,3)+shift(3),coilDrawProperties{:},coilDrawPropertiesLine{:});
    end
else
    positions   = vertcat(coils.Position);
    normals     = arrowScale .* vertcat(coils.Normal) ./ vecnorm(vertcat(coils.Normal),2,2);
    h = quiver3(positions(:,1)+shift(1),positions(:,2)+shift(2),positions(:,3)+shift(3),normals(:,1),normals(:,2),normals(:,3),0,coilDrawProperties{:},coilDrawPropertiesArrow{:});
end

if ~holdStatus; hold off; end


end

