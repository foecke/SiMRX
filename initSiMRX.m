%INITSIMRX adds toolbox subfolder to matlab path
%
%   run:
%       INITSIMRX
%


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function initSiMRX()

rootFolder = fileparts(mfilename('fullpath'));

addpath([rootFolder,'/configuration']);
addpath([rootFolder,'/configuration/buildTools']);
addpath([rootFolder,'/misc']);
addpath([rootFolder,'/misc/io']);
addpath([rootFolder,'/phantom']);
addpath([rootFolder,'/simulation']);
addpath([rootFolder,'/simulation/simTools']);
addpath([rootFolder,'/simulation/rawTools']);
addpath([rootFolder,'/visualization']);
addpath([rootFolder,'/visualization/drawTools']);
addpath([rootFolder,'/visualization/misc']);


end

