# *SiMRX* - A *Si*mulation toolbox for *MRX*

## Introduction
*SiMRX* is a MRX simulation toolbox written in MATLAB/Octave for simulation of realistic 2D and 3D MRX setups, including coils, sensors and activation patterns.  
It is based on the mathematical model presented in [[Foecke et al., 2018](http://iopscience.iop.org/article/10.1088/1361-6420/aadbbf)].

Detailed information about *SiMRX* are available at [https://arxiv.org/abs/1810.02286](https://arxiv.org/abs/1810.02286).

## Authors
* Lea Föcke<sup>1,2</sup> ([lea.foecke@fau.de](mailto:lea.foecke@fau.de))

### Acknowledgements and Contributions
This toolbox was inspired by earlier work of Peter Hoemmen<sup>4</sup>, Paul Koenigsberger<sup>4</sup> and Daniel Baumgarten<sup>3,4</sup>.
In particular for the simulation of the 3D case, we recycled code fragments originally created by Peter Hoemmen<sup>4</sup> and Paul Koenigsberger<sup>4</sup>.
Moreover, the 3D simulation step has been validated using a measured 3D dataset provided by Maik Liebl<sup>5</sup>.
The phantoms were created as part of a collaboration with Daniel Baumgarten<sup>3,4</sup> and Peter Schier<sup>3</sup>.
As part of the ongoing collaboration with Daniel Baumgarten<sup>3,4</sup> and Peter Schier<sup>3</sup> this code may receive updates and new features in the future.

Maik Liebl<sup>5</sup> has been supported by the German Science Foundation (DFG) within the priority program SPP1681 (WI 4230/1-3).

This work has been supported by the German Science Foundation (DFG) within the priority program CoSIP, project CoS-MRXI (BA 4858/2-1, BU 2327/4-1).

<sup>1</sup> Institute for Analysis and Numerics, Westfälische Wilhelms-Universität Münster (WWU), DE  
<sup>2</sup> Applied Mathematics, Friedrich-Alexander Universität Erlangen-Nürnberg (FAU), DE  
<sup>3</sup> Institute of Electrical and Biomedical Engineering, Private University of Health Sciences, Medical Informatics and Technology (UMIT), Hall in Tirol, AT  
<sup>4</sup> Institute of Biomedical Engineering and Informatics, Technische Universität Ilmenau (TU Ilmenau), DE  
<sup>5</sup> Physikalisch-Technische Bundesanstalt (PTB), Berlin, DE
## License
*SiMRX* is copyright ©2017-2021 by Lea Föcke. If you plan to distribute the software (commercially or not), please contact [Lea Föcke](mailto:lea.foecke@fau.de) for more information.

## Citation
If you use this toolbox (or a previous version) please use the following citation

```
@Article{Foecke2018SiMRX,
  author        = {Lea F{\"o}cke},
  title         = {{SiMRX} - A Simulation toolbox for {MRX}},
  journal       = {arXiv e-prints},
  year          = {2018},
  month         = oct,
  archiveprefix = {arXiv},
  eprint        = {1810.02286},
  keywords      = {magnetorelaxometry imaging; magnetic nanoparticles; modeling; {MATLAB} toolbox; {SiMRX}},
  primaryclass  = {cs.MS},
  url           = {https://arxiv.org/abs/1810.02286},
}
```

A preprint is available at [https://arxiv.org/abs/1810.02286](https://arxiv.org/abs/1810.02286).

## How to use?
* decide where to save the toolbox, e.g. `~/matlab/`
* download and copy the files into `~/matlab/simrx/`
  
  or use git clone:
  ```bash
  cd ~/matlab/
  git clone https://gitlab.com/simrx/simrx/
  ```
* start matlab
* add the toolbox path to your matlab path by using 
  ```matlab
  addpath('~/matlab/simrx');
  ```
* execute 
  ```matlab
  initSiMRX
  ```
  in the MATLAB command window once or add the command to the top of your MATLAB script. By doing so all subfolder of *SiMRX* are added to your path automatically and are ready for use

Please see the example privided in the `./examples` subfolder.

## Octave support

Please note, SiMRX is a MATLAB toolbox, however Octave support is currently in developement. Checkout `devOctave` for the most recent ported version.
  ```bash
  git checkout devOctave
  ```


## Dependencies
### Matlab: 

* version R2019a

### Octave:

* version 5.1
* linear-algebra package

## Changelog

## <u>v1.4</u>:

major changes:

* included measurement patterns into configs
* overhauled the phantom creation tool (also added versioning of phantoms)
* updated example setups to new structure
* moved raw tools into separate module
* separated pattern based and coil based system matrix creation
* reworked visualization module into single function

minor changes:

* updated contact information
* added idealized coil calibration tools
* added tool to cleanup activation and measurement patterns
* renamed/added script examples
* added input/output-tools
* added script rawDataLoader for quick and easy simulation management
* added function to simulate (multiple) measurements autonomously

## <u>v1.3</u>:
major changes:

* introduced general xyz coodinate system convention
* introduced setup/config folder structure
* added system for raw export/import. This allows for matrix assembly/calculation in runtime.
* promote setups as separate structure and moved into dedicated folder

minor changes:

* updated contact information
* removed configuration/add*.m functions for a more flexible approach
* moved config build tools in dedicated folder
* generally renamed "activation pattern" to "current pattern"
* added config and setup validation scripts
* added proper example scripts that make use of the introduced setup folder structure
* added setups that use script files to create data
* updated phantom creation to be have proper xyz orientation
* clean up in the magnetic field calculations - removed redundant code
* added dedicated functions for dipole and coil field creation
* separation of handle based draw function and assembled visualization tools
* general improvements in visualization tools


## <u>v1.2</u>:
* added examples that can be loaded from textbased data
* updated 2D and 3D examples
* renamed coilSequence to activationPattern (as of now addCoilSequence.m is a redirect to addActivationPattern.m to keep old code alive - however, this redirect will be removed with the next version)
* overhauled the visualization tools
* fixed many small bugs in visualization
* updated affiliation information
* added changelog to README
* updated tech report style
* added seperate files for each section of the report


## <u>v1.1.2</u>:  
* added script to compare setups
* fixed scaling of normal vector in simulation step
* added many properties for visualization tools


## <u>v1.1.1</u>: 
* updated funding information in README


## <u>v1.1</u>: 
* removed script, that required unavailable data.
* updated funding information.


## <u>v1.0</u>: Initial release
