%RAWDATALOADER is a useful loader for MRX data. The function creates all
%   necessary data in the raw format and exports it to the respective setup
%   folder. In case raw data are already available, these are loaded to
%   compile the system matrix, instead of simulating it again. Only coils
%   that are marked as active by the given config are simulated. The given
%   phantom is simulated for the given setup+config+resolution and is
%   exported to the setup folder as well. So in case a measurement is
%   already done for a given setup the measurement data are loaded from
%   file. The input data is structured like this:
%
%   setupPath       : path to setup, e.g. '~/matlab/SiMRX/setups'
%   setupName       : setup name
%   setupVariant    : setup variant (for different coil profiles)
%   configName      : config name
%   configVariant   : config variant
%   phantom.shape   : used phantom, e.g. 'shepplogan3d'
%   phantom.res     : phantom resolution
%   phantom.options : modfier flags for phantom creation
%   phantom.XMNP    : "amount" in phantom, e.g. 100, then sum(phantom(:))=100
%   resRecon        : reconstruction resolution
%
%   Note: It is possible to simulate mutliple phantoms at once by using an
%   struct array for phantom:
%   phantom(1).shape    = ...
%   phantom(1).res      = ...
%   phantom(1).options  = ...
%   phantom(2).shape    = ...
%   phantom(2).res      = ...
%
%
%   The output data is structured as follows:
%
%   A               : system matrix for given setup/config/resolution
%   measurement     : measured data for each phantom used
%   res             : reconstruction resolution
%   data.phantom    : properties of all used phantoms
%   data.setup      : used setup struct
%   data.rawPath    : raw path the raw data is exported to
%   data.config     : used config struct
%   data.u          : unknown or secret information ground truth phantom
%                       and clim information


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-Mai-2021


function dataOut = rawDataLoader(dataIn)


dataOut.res             = dataIn.resRecon;
dataOut.data.phantom   	= dataIn.phantom;
dataOut.data.rawPath   	= sprintf('%s/%s/raw/%s',dataIn.setupPath,dataIn.setupName,dataIn.setupVariant);


%% set up paths for measurement data
fpo('Load Setup [Variant]: ');
dataOut.data.setup   	= loadSetup(sprintf('%s/%s/%s.mrxsetup',dataIn.setupPath,dataIn.setupName,dataIn.setupVariant));
fprintf('%s [%s]\n', dataIn.setupName, dataIn.setupVariant);

fpo('Load Config [Variant]: ');
dataOut.data.config    	= loadConfig(sprintf('%s/%s/configs/%s/%s.mrxcfg', dataIn.setupPath,dataIn.setupName,dataIn.configName,dataIn.configVariant));
fprintf('%s [%s]\n', dataIn.configName, dataIn.configVariant);

prefixSpacing = floor(log10(numel(dataOut.data.phantom))) + 1;
prefixFormatString = ['(%0', num2str(prefixSpacing), '.f/%0', num2str(prefixSpacing), '.f) '];
numPhantoms = numel(dataOut.data.phantom);
fprintf('\n');


%% check for missing measurements
fpon('Check and Create Measurements: ');

listToImport = false(1,numPhantoms);
listIndex = 1:numPhantoms;

for i = listIndex
    if numPhantoms == 1
        countStrPrefix = '';
    else
        countStrPrefix = sprintf(prefixFormatString, i, numel(dataOut.data.phantom));
    end
    
    fpo(sprintf('%sLoad Ground Truth: ', countStrPrefix));
    dataOut.data.u(i).groundTruth    = createPhantom(dataOut.res, dataOut.data.phantom(i).shape, 'options', dataOut.data.phantom(i).options);
    dataOut.data.u(i).cLim           = [min(dataOut.data.u(i).groundTruth(:)),max(dataOut.data.u(i).groundTruth(:))];
    fprintf('####################\n');
    
    fpo(sprintf('%sLoad Simulation Phantom: ', countStrPrefix));
    [dataOut.data.phantom(i).imMeas, dataOut.data.phantom(i).uniqueName] = createPhantom(dataOut.data.phantom(i).res, dataOut.data.phantom(i).shape, 'options', dataOut.data.phantom(i).options);
    fprintf('####################\n');
    
    gMeasPath = sprintf('%s/%s/measurements/%s/%s/%s/%i.%i.%i', dataIn.setupPath, dataIn.setupName, dataIn.setupVariant, dataIn.configName, dataIn.configVariant, dataOut.data.phantom(i).res(:));
    
    fpo(sprintf('%sCheck Stored Measured Data: ', countStrPrefix));
    if exist([gMeasPath, '/', dataOut.data.phantom(i).uniqueName,'.mat'], 'file')
        fprintf('Stored Data Available\n');
    else
        listToImport(i) = true;
        fprintf('Missing: Added to Todo List\n');
    end
end


%% simulate and save missing measurement data
if any(listToImport)
    
    gCell  = deriveMeasurement(dataOut.data.setup, dataOut.data.config, {dataOut.data.phantom(listToImport).imMeas});
    
    tempList = listIndex(listToImport);
    for i = 1:numel(gCell)
        g = gCell{i};
        if numPhantoms == 1
            countStrPrefix = '';
        else
            countStrPrefix = sprintf(prefixFormatString, tempList(i), numel(dataOut.data.phantom));
        end
        
        gMeasPath = sprintf('%s/%s/measurements/%s/%s/%s/%i.%i.%i', dataIn.setupPath, dataIn.setupName, dataIn.setupVariant, dataIn.configName, dataIn.configVariant, dataOut.data.phantom(tempList(i)).res(:));
        if ~exist(gMeasPath, 'dir')
            mkdir(gMeasPath);
        end
        fpo(sprintf('%sSave Measured Data: ', countStrPrefix));
        if isOctave()
            save([gMeasPath, '/', dataOut.data.phantom(tempList(i)).uniqueName,'.mat'], '-mat7-binary', 'g');
        else
            save([gMeasPath, '/', dataOut.data.phantom(tempList(i)).uniqueName,'.mat'], '-mat', 'g');
        end
        fprintf('####################\n');
        
    end
end
fprintf('\n');


%% load measurement
fpon('Load Measurements: ');
for i = listIndex
    
    if numPhantoms == 1
        countStrPrefix = '';
    else
        countStrPrefix = sprintf(prefixFormatString, i, numel(dataOut.data.phantom));
    end
    
    gMeasPath = sprintf('%s/%s/measurements/%s/%s/%s/%i.%i.%i', dataIn.setupPath, dataIn.setupName, dataIn.setupVariant, dataIn.configName, dataIn.configVariant, dataOut.data.phantom(i).res(:));
    
    fpo(sprintf('%sLoad Measurement: ', countStrPrefix));
    load([gMeasPath, '/', dataOut.data.phantom(i).uniqueName,'.mat'], 'g');
    dataOut.measurement(i).g = g;
    fprintf('####################\n');
end

if numPhantoms == 1
    dataOut.g = dataOut.measurement(1).g;
end
fprintf('\n');


%% load reconstruction operator
fpon('Load System Matrix: ');
if ~isRawValid(dataOut.data.setup, dataOut.data.rawPath, dataOut.res, dataOut.data.config)
    rawExportSetupPar(dataOut.data.setup, dataOut.data.rawPath, dataOut.res, dataOut.data.config);
end
dataOut.A  = rawImportMatrix(dataOut.data.rawPath, dataOut.res, dataOut.data.config);

fprintf('\n\n');


end
