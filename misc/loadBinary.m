%data = LOADBINARY(path) loads a dataset in the provided path.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function data = loadBinary(path)

fileID = fopen(path);
data = fread(fileID, 'double');
fclose(fileID);

end