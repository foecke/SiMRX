%PARSAVEBINARY(path, data) stores the dataset data in path.


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen
%   Date:       19-May-2021


function parsaveBinary(path, data)

fileID = fopen(path,'w');
fwrite(fileID,data, 'double');
fclose(fileID);

end