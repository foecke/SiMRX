%FPO(s) (Formatted Print Out) prints the given string and fills up until
%   the printed string has a length of 50.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function fpo(s)
if nargin==0
    s='';
end
fprintf('%-50s',s);
end