%LOGGER is a class to handle printouts and logs with verbose level.
%
%   obj = LOGGER(logLevel) creates a new LOGGER instance with a given
%   logLevel. If no logLevel is provided it defaults to logLevel 1.
%
%   The following table shows what verboseLevel is required to print for a
%   logger with a certain logLevel:
%
%   | __ verboseLevel | WARNING (0) | INFO (1) | DEBUG (2) |
%   |    ''--__       |             |          |           |
%   | logLevel  ''--  |             |          |           |
%   | --------------- | ----------- | -------- | --------- |
%   | 0 / NONE        |             |          |           |
%   | 1 / WARNINGS    | x           |          |           |
%   | 2 / INFO        | x           | x        |           |
%   | 4 / DEBUG       | x           | x        | x         |
%
%   public functions:
%       setLogLevel(obj, logLevel)      sets new logLevel of LOGGER
%       print(obj, verboseLevel, s1)    prints string s1 if verboseLevel is
%                                       is high enough for current logLevel
%       print(obj, verboseLevel, s1, s2)    prints string s1, fills up to
%                                       50 printed symbols, and prints s2.
%       print(obj, verboseLevel, s1, s2, fSpacing)    change size of
%                                       whitespace between s1 and s2.
%


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


classdef logger < handle
    
    properties (Access = public)
        logLevel;
        logPrefix;
    end
    properties (Access = private)
        
        levelTable = [ 0 0 0;...
            1 0 0;...
            1 1 0;...
            1 1 1];
        verboseLevelOutputNames = { 'WARNING: ','INFO:    ','DEBUG:   '};
    end
    
    methods (Access = public)
        
        function obj = logger(logLevel)
            if nargin>0
                obj.logLevel = logLevel;
            else
                obj.logLevel = 1;
            end
            obj.logPrefix = '';
        end
        
        function setLogLevel(obj, logLevel)
            obj.logLevel = obj.parseLogLevel(logLevel);
        end
        
        function print(obj, verboseLevel, s1, s2, fSpacing)
            verboseLevel = obj.parseVerboseLevel(verboseLevel);
            if obj.checkLevelTable(verboseLevel)
                if nargin < 4
                    s2 = '';
                end
                if nargin < 5
                    fSpacing = 50;
                end
                s11 = sprintf(s1);
                s22 = sprintf(s2);
                if numel(fSpacing)==1
                    fSpace = fSpacing;
                else
                    cws = get(0, 'CommandWindowSize');
                    rOffset = cws(1) - length(s22) - 1;
                    fSpace = max(0, min(floor(cws(1).*fSpacing(1)./sum(fSpacing)),rOffset));
                end
                
                splitString = split(sprintf(['%-',num2str(max((length(s11) + 1), fSpace)),'s%s'],s11,s22), newline);
                for i = 1:numel(splitString)
                    if ~isempty(splitString{i})
                        fprintf('%s%s%s\n',obj.logPrefix,obj.verboseLevelOutputNames{verboseLevel+1},splitString{i});
                    end
                end
            end
        end
        
    end
    
    methods (Access = private)
        
        function logLevelOut = parseLogLevel(~, logLevelIn)
            switch lower(logLevelIn)
                case {0, 'none'}
                    logLevelOut = 0;
                case {1, 'w', 'warning', 'warnings'}
                    logLevelOut = 2;
                case {2, 'info'}
                    logLevelOut = 3;
                case {3, 'debug'}
                    logLevelOut = 4;
                otherwise
                    error('unable to parse given logLevel');
            end
        end
        
        function verboseLevelOut = parseVerboseLevel(~, verboseLevelIn)
            switch lower(verboseLevelIn)
                case {0, 'w', 'warning', 'warnings'}
                    verboseLevelOut = 0;
                case {1, 'info'}
                    verboseLevelOut = 1;
                case {2, 'debug'}
                    verboseLevelOut = 2;
                otherwise
                    error('unable to parse given verboseLevel');
            end
        end
        
        function out = checkLevelTable(obj, verboseLevel)
            out = obj.levelTable(obj.logLevel+1, verboseLevel+1);
        end
        
    end
end


