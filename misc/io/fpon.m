%FPON(s) (Formatted Print Out Newline) prints the given string and fills up
%   until the printed string has a length of 50. At the end newline is
%   printed.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021

function fpon(s)
if nargin==0
    s='';
end
fprintf('%s\n',s);
end