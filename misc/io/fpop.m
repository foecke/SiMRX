%FPOP(s) (Formatted Print Out Pair) prints the given string and fills up
%   until the first string has a length of 50. Then prints the second
%   string and ends with a newline.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function fpop(s1, s2)
if nargin<1
    s1='';
end
if nargin<2
    s2='';
end
fprintf('%-50s%s\n',s1, s2);
end