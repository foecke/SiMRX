%DECORATEDBOX prints a text and surrounds it with a visual box.
%
%   DECORATEDBOX(text) prints the text and surrounds it with #
%
%   DECORATEDBOX(text, boxSymbol) prints the text and surrounds it with the
%   provided symbol


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       28-Aug-2019


function decoratedBox(text, boxSymbol)

if nargin < 2
    boxSymbol = '#';
end

fprintf('\n%s\n%s %s %s\n%s\n\n', ...
    repmat(boxSymbol,1,length(text)+6), ...
    repmat(boxSymbol,1,2), ...
    text, ...
    repmat(boxSymbol,1,2), ...
    repmat(boxSymbol,1,length(text)+6));

end

