%PARSAVE saves a variable even within a parfor loop
%
%   if no variable name is given, it stores the provided data with
%   variablename 'data'


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen
%   Date:       19-May-2021


function parsave(path, data, variableName)

if nargin < 3
    variableName = 'data';
end

S.(variableName) = data;
if isOctave()
    save(path, '-mat7-binary', '-struct', 'S');
else
    save(path, '-mat', '-struct', 'S');
end

end
