%RAWIMPORTSETUP imports a raw dataset from path.
%
%   ARaw = RAWIMPORTSETUP(rawPath, res) imports a dataset available in
%   rawPath with resolution res, which is assembled to a matrix ARaw.
%
%   ARaw = RAWIMPORTSETUP(rawPath, res, config) only imports data that are
%   required for given config.


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function ARaw = rawImportSetup(rawPath, res, varargin)

%% parse inputs
p = inputParser;
p.addRequired('rawPath', @ischar);
p.addRequired('res', @isnumeric);
p.addOptional('config', [], @isstruct);
p.parse(rawPath, res, varargin{:});

rawPath         = p.Results.rawPath;
res             = p.Results.res;
config          = p.Results.config;

if isempty(config)
    coilList    = [];
    sensorList  = [];
else
    coilList    = config.coilsActive;
    sensorList  = config.sensorsActive;
end


%% function
rawPathRes = sprintf('%s/%i.%i.%i',rawPath,res(:));

if isempty(coilList)
    folderList  = dir(rawPathRes);
    dInd        = [folderList(:).isdir];
    folderNames = {folderList(dInd).name}';
    folderNames(ismember(folderNames,{'.','..'})) = [];
else
    if isOctave()
        folderNames = cell(1,numel(coilList));
        for i = 1:numel(coilList)
            folderNames{i} = sprintf('coil.%04d', coilList(i));
        end
    else
        folderNames = compose('coil.%04d', coilList);
    end
end

numOfCoils = numel(folderNames);
if numOfCoils==0
    error('raw folder contains no simulated data!');
end
ARaw = cell(numOfCoils,1);

fpo('Import Raw: ');
numOfPrintFeedback  = 20;
printInterval       = ceil(linspace(1,numOfCoils,numOfPrintFeedback+1));
printInterval       = unique(printInterval(2:end));

for i=1:numOfCoils
    curCoilFolder = [rawPathRes, '/', folderNames{i}];
    if ~exist(sprintf('%s/allSensors.dat',curCoilFolder), 'file')
        error('file %s/allSensors.dat does not exist!',curCoilFolder);
    end
    if isempty(sensorList)
        ARaw{i} = reshape(loadBinary(sprintf('%s/allSensors.dat',curCoilFolder)), [], prod(res));
    else
        for j=1:numel(sensorList)
            curSensor = sensorList(j);
            if exist(sprintf('%s/sensor.%04d.dat',curCoilFolder,curSensor), 'file')
                ARaw{i}(j,:) = reshape(loadBinary(sprintf('%s/sensor.%04d.dat',curCoilFolder,curSensor)), [], prod(res));
            else
                A = reshape(loadBinary(sprintf('%s/allSensors.dat',curCoilFolder)), [], prod(res));
                ARaw{i} = A(sensorList,:);
                break;
            end
        end
    end
    
    if any(i==printInterval)
        fprintf('#');
    end
end

for i = 1:(numOfPrintFeedback-numel(printInterval))
    fprintf('#');
end
fprintf('\n');


end

