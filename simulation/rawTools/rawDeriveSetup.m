%RAWDERIVESETUP derives raw files.
%
%   ARaw = RAWDERIVESETUP(setup, config, res) derives raw files for a given
%   setup, config and resolution. ARaw is a cell array with one entry for
%   each coil given in config.coilsActive. The measurements have been
%   performed on config.sensorsActive only.


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function ARaw = rawDeriveSetup(setup, config, res)

%% check setup and config compatibility
if ~checkCompatibility(setup, config); error('setup and config are not compatible'); end


%% create voxelgrid
voxelGrid = createVoxelGrid(setup.roi, res);


%% calculate magnetic fields induced by coils
magnFields = createExcitationFields(setup.coils(config.coilsActive), voxelGrid);


%% calculate dipole induced fields
ARaw = createRelaxationFields(setup.sensors(config.sensorsActive), magnFields, voxelGrid);


end

