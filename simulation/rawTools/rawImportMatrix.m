%RAWIMPORTMATRIX imports a system matrix from previously exported raw files
%
%   A = RAWIMPORTMATRIX(rawPath, res, config) imports a prederived setup
%   provided in rawPath with resolution res and given config.
%
%   A = RAWIMPORTMATRIX(..., varargin) allows optional
%   settings/parameter:
%       'verbose'       true or false. Show progress bar.


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function A = rawImportMatrix(rawPath, res, config, varargin)

%% parse inputs
p = inputParser;
p.addRequired('rawPath', @ischar);
p.addRequired('res', @isnumeric);
p.addRequired('config', @isstruct);
p.addOptional('verbose', true, @islogical);
p.parse(rawPath, res, config, varargin{:});

rawPath     = p.Results.rawPath;
res         = p.Results.res;
config      = p.Results.config;
verbose     = p.Results.verbose;


%% function
n = prod(res);
numOfPatterns   = size(config.currentPattern, 1);
numOfCoils      = numel(config.coilsActive);

A = zeros([nnz(config.measurementPattern),n]);
AOffset = 0;                                                                % define offset, since not every pattern has the same number of measurements

if verbose
    fpo('Import Matrix: ');
    numOfPrintFeedback = 20;
    printInterval = ceil(linspace(1,numOfPatterns,numOfPrintFeedback+1));
    printInterval = unique(printInterval(2:end));
end

for i = 1:numOfPatterns
    currentCurrentPattern       = config.currentPattern(i, :);           	% set current current pattern
    currentMeasurementPattern   = logical(config.measurementPattern(i, :)); % set current measurement pattern
    curNumOfSensors             = nnz(currentMeasurementPattern);           % get active number of sensors for current pattern
    ATemp = zeros([curNumOfSensors,n]);                                     % define size of this patterns system matrix
    for j = 1:numOfCoils                                                    % loop through all coils to check for activations
        if currentCurrentPattern(j) ~= 0                                    % check if current coil is active
            curCoil         = config.coilsActive(j);                        % set current coil
            curCoilFolder   = sprintf('%s/%i.%i.%i/coil.%04d',rawPath,res(:), curCoil);
            ARaw            = reshape(loadBinary(sprintf('%s/allSensors.dat',curCoilFolder)), [], n);
            % add respective ARaw to this activations system matrix
            ATemp           = ATemp + ARaw(config.sensorsActive(currentMeasurementPattern),:) .* currentCurrentPattern(j);
        end
    end
    A(AOffset+1:AOffset+curNumOfSensors,:) = ATemp;                         % implant complete system matrix for this pattern in full matrix
    AOffset = AOffset + curNumOfSensors;                                    % update offset
    
    if verbose && any(i==printInterval)
        fprintf('#');
    end
end

if verbose
    for i = 1:(numOfPrintFeedback-numel(printInterval))
        fprintf('#');
    end
    fprintf('\n');
end


end

