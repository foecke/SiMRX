%RAWIMPORTMAGNETICFIELDS imports magnetic field data from previously
%exported raw files.
%
%   fields = RAWIMPORTMAGNETICFIELDS(rawPath, res) imports all fields
%   available in rawPath with resolution res.
%
%   fields = RAWIMPORTMAGNETICFIELDS(rawPath, res, config) only imports
%   data that are required for given config.
%
%   fields = RAWIMPORTMAGNETICFIELDS(..., varargin) allows optional
%   settings/parameter:
%       'coilList'      list of coils to import. It is ignored if a
%                       config file is provided. In this case
%                       config.coilsActive is used.


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function magnFields = rawImportMagneticFields(rawPath, res, varargin)

%% parse inputs
p = inputParser;
p.addRequired('rawPath', @ischar);
p.addRequired('res', @isnumeric);
p.addOptional('config', [], @isstruct);
p.addParameter('coilList', 0);
p.parse(rawPath, res, varargin{:});

rawPath         = p.Results.rawPath;
res             = p.Results.res;
config          = p.Results.config;
coilList        = p.Results.coilList;

if ~isempty(config)
    coilList    = config.coilsActive;
end


%% function
rawPathRes = sprintf('%s/%s', rawPath, strjoin(string(res), '.'));

if coilList == 0
    folderList  = dir(rawPathRes);
    dInd        = [folderList(:).isdir];
    folderNames = {folderList(dInd).name}';
    folderNames(ismember(folderNames,{'.','..'})) = [];
else
    folderNames = compose('coil.%04d', coilList);
end

numOfCoils = numel(folderNames);
magnFields = cell(numOfCoils,1);

for i=1:numOfCoils
    curCoilFolder = [rawPathRes, '/', folderNames{i}];
    
    magnField = loadBinary(sprintf('%s/magnField.dat',curCoilFolder));
    magnFields{i} = reshape(magnField, [], 3);
end


end

