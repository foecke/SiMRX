%ISRAWVALID checks if an exported raw folder contains all necessary
%   information
%
%   integrity = ISRAWVALID(setup, rawPath, res) checks the folder rawPath
%   contains a simulated dataset of setup with the given resolution.
%
%   integrity = ISRAWVALID(setup, rawPath, res, config) only checks for
%   datasets that are required for the usage of given config.
%
%   integrity = ISRAWVALID(..., varargin) allows optional
%   settings/parameter:
%       'type'          defines the what data is checked: (default: 'af')
%                       'allSensor','allsensors' or 'a': check if all
%                           sensors are simulated and stored in a
%                           'allSensors.mat' file.
%                       'field','fields','f': check if the simulated
%                           excitation field of each coil is saved in
%                           'magnField.mat'.
%                       'af': combines the settings 'a' and 'f', hence
%                           checks for 'allsensors.mat' and 'magnField.mat'
%                           (default).
%                       'singlesensor','singlesensors' or 's': checks if
%                           every sensors is saved seperately.
%                       'complete','full' or 'c': combination of 'a', 'f'
%                           and 's'.
%       'coilList'      list of coils to check. It is ignored if a
%                       config file is provided. In this case
%                       config.coilsActive is used.
%       'sensorList'    list of sensors to check. It is ignored if a
%                       config file is provided. In this case
%                       config.sensorsActive is used. It is also ignored,
%                       if type 'a', 'af' or 'c' is set.
%
%   [integrity, coilListRedo, sensorListRedo] = ISRAWVALID(___) returns a
%   boolean integrity as well as list of coils and sensors that are not
%   valid under the given conditions and need to be exported again using
%   RAWEXPORTSETUP.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function [integrity, coilListRedo, sensorListRedo] = isRawValid(setup, rawPath, res, varargin)

%% parse inputs
p = inputParser;
p.addRequired('setup', @isstruct);
p.addRequired('rawPath', @ischar);
p.addRequired('res', @isnumeric);
p.addOptional('config', [], @isstruct);
p.addParameter('type', 'af', @(s) any(strcmp({'complete','c','full','allsensor','allsensors','a','singlesensor','singlesensors','s','field','fields','f','af'}, s)));
p.addParameter('coilList', 0);
p.addParameter('sensorList', 0);
p.parse(setup, rawPath, res, varargin{:});

setup       = p.Results.setup;
rawPath     = p.Results.rawPath;
res         = p.Results.res;
config      = p.Results.config;
type        = p.Results.type;
coilList    = p.Results.coilList;
sensorList  = p.Results.sensorList;

if ~isempty(config)
    coilList    = config.coilsActive;
    sensorList  = config.sensorsActive;
end


%% check integrity of raw data
if coilList==0
    coilList = 1:numel(setup.coils);
end
numOfCoils = numel(coilList);
if sensorList==0
    sensorList = 1:numel(setup.sensors);
end
numOfSensors = numel(sensorList);

integrityCoils          = true(1,numOfCoils);
integritySingleSensors  = true(numOfSensors,numOfCoils);
integrityFields         = true(1,numOfCoils);
integrityAllSensors     = true(1,numOfCoils);

checkSingleSensors      = false;
checkFields             = false;
checkAllSensors         = false;

switch type
    case {'complete','c','full'}
        checkSingleSensors  = true;
        checkFields         = true;
        checkAllSensors     = true;
    case {'af'}
        checkFields         = true;
        checkAllSensors     = true;
    case {'allsensor','allsensors','a'}
        checkAllSensors     = true;
    case {'singlesensor','singlesensors','s'}
        checkSingleSensors  = true;
    case {'field','fields','f'}
        checkFields         = true;
end

coilListRedo    = [];
sensorListRedo  = [];


%% function
curPath = sprintf('%s/%i.%i.%i',rawPath,res(:));

warning on all
warning off backtrace
warning off verbose

if ~exist(curPath, 'dir')
    warning('%s does not exist!', curPath);
    coilListRedo    = coilList; % all
    sensorListRedo  = sensorList; % all
    integrity       = false;
    return;
else
    for j = 1:numOfCoils
        curCoil = coilList(j);
        
        % as of Nov 2019, the function string is not yet implented in octace,
        % hence the following workaround
        curPath = sprintf('%s/%i.%i.%i/coil.%04d',rawPath,res(:),curCoil);
        
        if ~isfolder(curPath)
            integrityCoils(j) = false;
            warning('%s does not exist!', curPath);
            coilListRedo    = unique([coilListRedo, curCoil]); % add current coil
            sensorListRedo  = sensorList; % all
        else
            if checkAllSensors
                curFile = [curPath, '/allSensors.dat'];
                if ~exist(curFile, 'file')
                    integrityAllSensors(j) = false;
                    warning('%s does not exist!', curFile);
                    coilListRedo    = unique([coilListRedo, curCoil]); % add current coil
                    sensorListRedo  = sensorList; % all
                else
                    fileInfo = dir(curFile);
                    if fileInfo.bytes == 0
                        integrityAllSensors(j) = false;
                        warning('%s is damaged!', curFile);
                        coilListRedo	= unique([coilListRedo, curCoil]); % add current coil
                        delete(curFile);
                    end
                end
            end
            if checkFields
                curFile = [curPath, '/magnField.dat'];
                if ~exist(curFile, 'file')
                    integrityFields(j) = false;
                    warning('%s does not exist!', curFile);
                    coilListRedo	= unique([coilListRedo, curCoil]); % add current coil
                else
                    fileInfo = dir(curFile);
                    if fileInfo.bytes == 0
                        integrityFields(j) = false;
                        warning('%s is damaged!', curFile);
                        coilListRedo	= unique([coilListRedo, curCoil]); % add current coil
                        delete(curFile);
                    end
                end
            end
            if checkSingleSensors
                for i=1:numOfSensors
                    curSensor = sensorList(i);
                    curFile = sprintf('%s/sensor.%04d.dat',curPath,curSensor);
                    if ~exist(curFile, 'file')
                        integritySingleSensors(i,j) = false;
                        warning('%s does not exist!', curFile);
                        coilListRedo	= unique([coilListRedo, curCoil]); % add current coil
                        sensorListRedo  = unique([sensorListRedo, curSensor]); % add current sensor
                    else
                        fileInfo = dir(curFile);
                        if fileInfo.bytes == 0
                            integritySingleSensors(i,j) = false;
                            warning('%s is damaged!', curFile);
                            coilListRedo	= unique([coilListRedo, curCoil]); % add current coil
                            sensorListRedo  = unique([sensorListRedo, curSensor]); % add current sensor
                            delete(curFile);
                        end
                    end
                end
            end
        end
    end
end
warning on all
warning on backtrace
integrity = all([integrityCoils(:);integrityAllSensors(:);integrityFields(:);integritySingleSensors(:)]);


end

