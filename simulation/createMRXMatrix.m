%CREATEMRXMATRIX simulates MRX system based on a given setup and config.
%
%   A = CREATEMRXMATRIX(setup, config, res) simulates the MRX system and
%   applies a coil current sequence.
%   CREATEMRXMATRIX can handle 2D and 3D setups.
%
%   run
%       CREATEMRXMATRIX
%
%   to see a minimal example


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       06-Apr-2018


function A = createMRXMatrix(setup, config, res, doRaw)

if nargin < 4
    doRaw = false;
end

if doRaw % create A using raw files
    A   = rawDeriveSetup(setup, config, res);
else % create A one pattern after another (usually faster)
    A 	= createMRXMatrixByPattern(setup, config, res);
end


end

