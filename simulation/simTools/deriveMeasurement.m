%DERIVEMEASUREMENT derives a measurement.
%
%   g = DERIVEMEASUREMENT(setup, config, phantom) derives the measurement
%   for a given setup with config. The particle distribution is given by
%   phantom.
%
%   g = DERIVEMEASUREMENT(..., verbose) allows to control the output of a
%   progress bar.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       22-May-2021


function g = deriveMeasurement(setup, config, phantom, verbose)

%% check setup and config compatibility
if ~checkCompatibility(setup, config); error('setup and config are not compatible'); end


%% derive system matrix for each pattern
if nargin < 4
    verbose = true;
end
inputIsCell = iscell(phantom);
if ~inputIsCell
    phantom = {phantom};
end

numOfImInputs = numel(phantom);
indexInputImages = 1:numOfImInputs;
for i = indexInputImages
    tempRes = size(phantom{i});
    tempRes = [tempRes, 1, 1, 1];
    resCell{i} = tempRes(1:3);
end
resStr = cellfun(@(x) num2str(x(:)'),resCell,'UniformOutput',false);
[~,ia,ic] = unique(resStr);
resUnique = resCell(ia);


numOfPatterns   = size(config.currentPattern, 1);
gTemp           = cell(numOfPatterns, numOfImInputs);
g               = cell(1, numOfImInputs);

for k = 1:numel(resUnique)
    curRes = resUnique{k};
    indexCurIm = indexInputImages(ic==k);
    
    if verbose
        fpo(sprintf('Derive Measurements [%i.%i.%i]: ', curRes(:)));
        numOfPrintFeedback = 20;
        printInterval = ceil(linspace(1,numOfPatterns,numOfPrintFeedback+1));
        printInterval = unique(printInterval(2:end));
    end
    
    for i = 1:numOfPatterns
        
        curConfig                       = config;
        
        curCurrentPattern               = config.currentPattern(i,:);
        curConfig.currentPattern        = config.currentPattern(i,curCurrentPattern>0);
        curConfig.coilsActive           = config.coilsActive(curCurrentPattern>0);
        
        curMeasurementPattern           = config.measurementPattern(i,:);
        curConfig.measurementPattern    = config.measurementPattern(i,curMeasurementPattern>0);
        curConfig.sensorsActive         = config.sensorsActive(curMeasurementPattern>0);
        
        ATemp    = createMRXMatrixByPattern(setup, curConfig, curRes, false);
        
        for j = indexCurIm
            gTemp{i,j} = ATemp * phantom{j}(:);
        end
        if verbose && any(i==printInterval)
            fprintf('#');
        end
    end
    
    if verbose
        for i = 1:(numOfPrintFeedback-numel(printInterval))
            fprintf('#');
        end
        fprintf('\n');
    end
end


for j = 1:numOfImInputs
    g{j} = cell2mat(gTemp(:,j));
end
if ~inputIsCell
    g = g{1};
end


end

