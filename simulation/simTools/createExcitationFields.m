%CREATEEXCITATIONFIELDS calculates magnetic fields induced by coils or
%dipoles.
%
%   magnFields = CREATEEXCITATIONFIELDS(coils, voxelGrid) derives the
%   magnetic fields in all points in voxelGrid for a given array of coils.
%   In case the coil has a segement struct added the discrete Biot-Savart
%   law is used to derive the fields as acomposition of each segements
%   magnetic fields. Otherwise the coil is treated as a dipole inducing
%   with a field strength corresponding to the length of the normal vector.
%
%   CREATEEXCITATIONFIELDS(..., verbose) allows to control the output of a
%   progress bar.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function magnFields = createExcitationFields(coils, voxelGrid, verbose)

if nargin<3
    verbose = true;
end
numOfCoils  = size(coils, 2);

magnFields  = cell(numOfCoils,1);

if verbose
    fpo('Derive Magnetic Fields: ');
    numOfPrintFeedback = 20;
    printInterval = ceil(linspace(1,numOfCoils,numOfPrintFeedback+1));
    printInterval = unique(printInterval(2:end));
end

for i = 1:numOfCoils
    if isfield(coils(i), 'Segments')
        magnFields{i} = createCoilField(coils(i), voxelGrid);
    else
        magnFields{i} = createDipoleField(coils(i), voxelGrid);
    end
    
    if verbose && any(i==printInterval)
        fprintf('#');
    end
end

if verbose
    for i = 1:(numOfPrintFeedback-numel(printInterval))
        fprintf('#');
    end
    fprintf('\n');
end


end

