%CREATECOILFIELD derives a magnetic field.
%
%   magnField = CREATECOILFIELD(coil, voxelGrid) derives the magnetic field
%   in all points in voxelGrid, where the coil is provided by the struct
%   coil. The coil is composed of straight segments, which are used to
%   derive the induced fields.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


%   This code is inspired by a codebase provided by Peter Hoemmen, 2015,
%   TU Ilmenau, Ilmenau, DE.
%   However the following code is a full reimplementation and the
%   general structure has been optimized heavily.


function magnField = createCoilField(coil, voxelGrid)

varphi      = 1e-7; % varphi = mu0 / 4pi = (1e-7 * 4pi) / 4pi = 1e-7

numOfVoxels = size(voxelGrid, 1);
magnField   = zeros(numOfVoxels, 3);


%%
for j = 1:numOfVoxels
    curVoxel    = voxelGrid(j,:);
    
    % precalculate all single segements
    rDiff       = coil.Segments - curVoxel(ones(size(coil.Segments,1),1),:);
    crossRDiff  = cross(rDiff(1:end-1,:), rDiff(2:end,:));
    dotRDiff    = dot(rDiff(1:end-1,:), rDiff(2:end,:),2);
    
    rDiffAbs    = sqrt(sum(rDiff.^2,2));
    rDiffSum    = rDiffAbs(1:end-1) +  rDiffAbs(2:end);
    rDiffProd   = rDiffAbs(1:end-1) .* rDiffAbs(2:end);
    
    tempField   = (rDiffSum./(rDiffProd.*(rDiffProd+dotRDiff)));
    magField    = sum(crossRDiff.*repmat(tempField, [1 3]));
    
    magnField(j,:) = varphi * magField;
end


end

