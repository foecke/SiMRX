%CREATEVOXELGRID creates a voxel grid for a given region of interest and
%given gridsize.
%
%   CREATEVOXELGRID(roi, res) creates a meshgrid where roi.x, roi.y and
%   roi.z are intervals that define the region of interest. The resolution
%   is defined by a 3D vector res.
%
%   CREATEVOXELGRID(roi, res, type) where the type specifies where the
%   grid is placed within the region of interest. 'boundary' and 'b'
%   respects the width of a voxel, so that the edges of the outer voxel do
%   not cross the region of interest. Then the created voxelgrid is defined
%   by the midpoints of these voxel. This is the default mode.
%
%   CREATEVOXELGRID(roi, res, type, cuts) where the output is limited to
%   the defined region defined by cuts.
%
%   just run:
%       CREATEVOXELGRID
%
%   to see a minimal example.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       22-May-2021


function [voxelGrid, cutMap] = createVoxelGrid(roi, res, type, cuts)

%% run example?
if nargin == 0
    [voxelGrid, cutMap] = runExample;
    return;
end


%% function

% make sure that res is 3 element vector
res = [res, 1, 1, 1];
res = res(1:3);

if nargin < 3 || isempty(type)
    type = 'b';
end
if nargin < 4 || isempty(cuts)
    cuts.x = [1, res(1)];
    cuts.y = [1, res(2)];
    cuts.z = [1, res(3)];
end

span    = cat(3, roi.x, roi.y, roi.z);
xyz     = cell(1,3);
cutxyz  = cell(1,3);

for i = 1:3
    if strcmp(type,'boundary') || strcmp(type,'b')
        temp = linspace(span(1,1,i), span(1,2,i), 2*res(i)+1);
        temp = temp(2:2:end);
    else
        temp = linspace(span(1,1,i), span(1,2,i), res(i));
    end
    switch i
        case 1
            curCuts = cuts.x;
        case 2
            curCuts = cuts.y;
        case 3
            curCuts = cuts.z;
    end
    xyz{i}      = temp(curCuts(1):curCuts(2));
    cutxyz{i}   = zeros(size(temp));
    cutxyz{i}(curCuts(1):curCuts(2)) = 1;
end


% X and Y is swapped to compensate for MATLAB column-mayor order
[Y, X, Z]   = meshgrid(xyz{2}, xyz{1}, xyz{3});
voxelGrid 	= cat(2,X(:),Y(:),Z(:));

[cutY, cutX, cutZ]   = meshgrid(cutxyz{2}, cutxyz{1}, cutxyz{3});
cutMap     	= all(cat(2,cutX(:),cutY(:),cutZ(:)),2);


%% example
    function [voxelGrid, cutMap] = runExample()
        
        res	= [11,5,3];
        roi.x       = [-1 1];
        roi.y       = [-1 1];
        roi.z       = [-1 1];
        type        = '-';
        cuts.x      = [3 7];
        cuts.y      = [1 5];
        cuts.z      = [1 1];
        
        [voxelGrid, cutMap] = createVoxelGrid(roi, res, type, cuts);
        
    end


end

