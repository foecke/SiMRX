%CREATEDIPOLEFIELD derives a magnetic field.
%
%   magnField = CREATEDIPOLEFIELD(dipole, voxelGrid) derives the magnetic
%   field in all points in voxelGrid, where the dipole is provided by the
%   struct dipole. The dipole is composed of a position vector and normal
%   vector.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


%   This code is inspired by a codebase provided by Peter Hoemmen and
%   Paul Koenigsberger, 2015, TU Ilmenau, Ilmenau, DE.
%   However the following code is a full reimplementation and the
%   general structure has been optimized heavily.


function magnField = createDipoleField(dipole, voxelGrid)

varphi      = 1e-7; % varphi = mu0 / 4pi = (1e-7 * 4pi) / 4pi = 1e-7

numOfVoxels	= size(voxelGrid, 1);
magnField   = zeros(numOfVoxels, 3);


%%
currObj     = dipole.Position;
currObjN    = dipole.Normal;
rDiff       = bsxfun(@minus,currObj,voxelGrid);
rDiffAbs    = sqrt(sum(rDiff.^2,2));

for j = 1:numOfVoxels
    magnField(j,:) = varphi * ((3*currObjN*(rDiff(j,:)'*rDiff(j,:)).*rDiffAbs(j,:).^(-5))-currObjN*(eye(3)*rDiffAbs(j,:).^(-3)));
end


end

