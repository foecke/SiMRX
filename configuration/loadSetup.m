%LOADSETUP loads a mrx setup from a file.
%   setup = LOADSETUP(filename) loads a mrx setup file. The .mrxsetup
%   extension is added automatically if missing.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function setup = loadSetup(filename)

%% parse inputs
p = inputParser;
p.addRequired('filename');

p.parse(filename);

filename = p.Results.filename;


%% function
if length(filename) < 9 || ~strcmp(filename(end-8:end),'.mrxsetup')
    filename = [filename,'.mrxsetup'];
end

if exist(filename, 'file')
    setup = load(filename, '-mat');
else
    error('%s does not exist', filename);
end

if ~isSetupValid(setup)
    warning('Loaded setup is not valid!');
end

end

