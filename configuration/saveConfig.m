%SAVECONFIG saves a mrx config to a config file.
%
%   SAVECONFIG(filename,config) saves config in filename. In case a file
%   already exist it prompts for furher instructions. SAVECONFIG
%   automatically adds .mrxcfg extension to filename.
%
%   SAVECONFIG(filename,config,varargin) allows optional setting/parameter:
%   	'override'      overrides filename in case a file already exist
%                       without promting.
%   	'ff'            fast forward - skips any promts


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function saveConfig(filename,config,varargin)

%% parse inputs
p = inputParser;
p.addRequired('filename');
p.addRequired('config');
p.addParameter('override', false, @islogical);
p.addParameter('ff', false, @islogical);

p.parse(filename,config,varargin{:});

filename        = [p.Results.filename,'.mrxcfg'];
override        = p.Results.override;
ff              = p.Results.ff;

currentPattern      = p.Results.config.currentPattern;
coilsActive         = p.Results.config.coilsActive;
sensorsActive       = p.Results.config.sensorsActive;
measurementPattern  = p.Results.config.measurementPattern;
if isfield(p.Results.config, 'info')
    info        = p.Results.config.info;
else
    info        = [];
end
if ~isfield(info, 'name')
    prompt = 'The setup name was not specified (setup.info.name is not set). Please type a name: ';
    
    info.name = [];
    while isempty(info.name)
        info.name = input(prompt,'s');
        if isempty(info.name)
            disp('the name may not be empty!')
        end
        if strcmpi(info.name, 'a name')
            disp('very funny, we try that again!');
            info.name = [];
        end
    end
end
if ~isfield(info, 'variant')
    prompt = 'The setup variant was not specified (setup.info.variant is not set). Please type a variant name: ';
    
    info.variant = [];
    while isempty(info.variant)
        info.variant = input(prompt,'s');
        if isempty(info.variant)
            disp('the variant name may not be empty!')
        end
        if strcmpi(info.variant, 'a variant name')
            disp('oh come on please!');
            info.variant = [];
        end
    end
end

skip = false;

%% check if config is valid
if ~isConfigValid(config)
    prompt = 'The config is not valid! Save nevertheless (NOT RECOMMENDED)? Y/N [N]: ';
    if ff
        str = 'N';
        disp([prompt, str]);
    else
        str = input(prompt,'s');
        if isempty(str)
            str = 'N';
        end
    end
    if ~any(strcmp(str,{'Y','y','yes'}))
        skip=true;
        fpop('config creation:','skipped!');
    end
end


%% add .mrxcfg if missing
if length(filename) < 7 || ~strcmp(filename(end-6:end),'.mrxcfg')
    filename = [filename,'.mrxcfg'];
end


%% create folder if not existent
filepath = fileparts(filename);
if ~exist(filepath,'dir')&&~isempty(filepath)
    prompt = ['folder ', filepath, ' does not exist! Create? Y/N [Y]: '];
    
    if ff
        str = 'Y';
        disp([prompt, str]);
    else
        str = input(prompt,'s');
        if isempty(str)
            str = 'Y';
        end
    end
    
    if any(strcmp(str,{'Y','y','yes'}))
        mkdir(filepath);
        fpop('folder creation:','done!');
    else
        skip=true;
        fpop('config creation:','skipped!');
    end
end


%% function
if exist(filename, 'file')&&~override
    prompt = [filename, ' already exist! Override? Y/N [N]: '];
    
    if ff
        str = 'N';
        disp([prompt, str]);
    else
        str = input(prompt,'s');
        if isempty(str)
            str = 'N';
        end
    end
    
    if ~any(strcmp(str,{'Y','y','yes'}))
        skip=true;
        fpop('config creation:','skipped!');
    end
end

if ~skip
    if isOctave()
        save(filename, '-mat7-binary', 'currentPattern', 'coilsActive', 'sensorsActive', 'measurementPattern', 'info');
    else
        save(filename, '-mat', 'currentPattern', 'coilsActive', 'sensorsActive', 'measurementPattern', 'info');
    end
    fpop('config creation:','done!');
end



end

