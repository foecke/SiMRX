% ISSETUPVALID checks if for a given setup is valid.
%
%   integrity = ISSETUPVALID(setup) checks if the provided setup is valid.
%   If all checks are passed successfully the function return true,
%   otherwise returns false.
%
%   The following checks are performed:
%       * dim is set
%       * roi is set
%       * coils is set
%       * sensors is set
%       * coilGroups is set
%       * sensorGroups is set
%       * info is set
%       * info.name is set
%       * info.variant is set
%       * dim is compatible to roi.z


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function integrity = isSetupValid(setup)

%% assume something goes wrong ;)
integrity = false;


%% check if all setup variables are set
if ~isfield(setup, 'dim'); warning('setup.dim not set!'); return; end
if ~isfield(setup, 'roi'); warning('setup.roi not set!'); return; end
if ~isfield(setup, 'coils'); warning('setup.coils not set!'); return; end
if ~isfield(setup, 'sensors'); warning('setup.sensors not set!'); return; end
if ~isfield(setup, 'coilGroups'); warning('setup.sensorGroups not set!'); return; end
if ~isfield(setup, 'sensorGroups'); warning('setup.sensorGroups not set!'); return; end
if ~isfield(setup, 'info'); warning('setup.info not set!'); return; end
if ~isfield(setup.info, 'name'); warning('setup.info.name not set!'); return; end
if ~isfield(setup.info, 'variant'); warning('setup.info.variant not set!'); return; end


%% check if roi.z and dim fits
if ~(xor(range(setup.roi.z)==0, setup.dim==3)); warning('setup.roi.z and setup.dim do not fit!'); return; end


%% template for a new rule
if ~(true); warning('something went wrong!'); return; end


%% return true if everything passed
integrity = true;


end

