%LOADEXAMPLEMRXDATASET loads a 2D or 3D example dataset
%
%	[setup, config] = LOADEXAMPLEMRXDATASET(dim) loads a default
%	example where the variable dim may be 2 or 3.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function [setup, config] = loadExampleMRXDataset(dim)

if nargin==0 || isempty(dim)
    dim = 3;
end
dim     = max(2,min(3,dim));

thisPath = fileparts(mfilename('fullpath'));
SiMRXPath = thisPath(1:find(thisPath==filesep, 1, 'last')-1);

setupPath = [SiMRXPath, '/setups/default',num2str(dim),'D/default.mrxsetup'];
if dim == 2
    configPath = [SiMRXPath, '/setups/default2D/configs/all/singleSequential.mrxcfg'];
else
    configPath = [SiMRXPath, '/setups/default3D/configs/all/singleSequential.mrxcfg'];
end

orig_state = warning;
if exist(setupPath, 'file')
    setup   = loadSetup(setupPath);
else
    warning('off', 'backtrace');
    error('\nThe default%iD example setup file does not exist! (%s)\nplease run ''README.m'' in <SiMRX-root>/setups/default%iD/ to create the required files.\n\n',dim,setupPath,dim);
end
if exist(configPath, 'file')
    config  = loadConfig(configPath);
else
    warning('off', 'backtrace');
    error('\nThe default%iD example config file does not exist! (%s)\nplease run ''README.m'' in <SiMRX-root>/setups/default%iD/ to create the required files.\n\n',dim,configPath,dim);
end
warning(orig_state);

end

