% CHECKCOMPATIBILITY checks if a given setup is compatible with the given
% config.
%
%   integrity = CHECKCOMPATIBILITY(setup, config) checks if the provided
%   setup and config are valid and are compatible. If all checks are
%   passed successfully the function return true, otherwise returns false.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function integrity = checkCompatibility(setup, config)

%% assume something goes wrong ;)
integrity = false;


%% check if setup is valid
if ~isSetupValid(setup); return; end
if ~isConfigValid(config); return; end


%% check if coilsActive and sensorsActive fit the setup
if ~(max(config.coilsActive)<=numel(setup.coils)); warning('config.coilsActive and setup.coils are not compatible!'); return; end
if ~(max(config.sensorsActive)<=numel(setup.sensors)); warning('config.sensorsActive and setup.sensors are not compatible!'); return; end


%% template for a new rule
if ~(true); warning('something went wrong!'); return; end


%% return true if everything passed
integrity = true;


end

