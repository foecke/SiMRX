%LOADCONFIG loads a mrx setup from a file.
%
%   setup = LOADCONFIG(filename) loads a mrx config file. The .mrxcfg
%   extension is added automatically if missing.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function config = loadConfig(filename)

%% parse inputs
p = inputParser;
p.addRequired('filename');

p.parse(filename);

filename = p.Results.filename;

%% function

if length(filename) < 7 || ~strcmp(filename(end-6:end),'.mrxcfg')
    filename = [filename,'.mrxcfg'];
end

if exist(filename, 'file')
    config = load(filename, '-mat');
else
    error('%s does not exist', filename);
end

if ~isConfigValid(config)
    warning('Loaded config is not valid!');
end

end

