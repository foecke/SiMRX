%SETCOILLOOPFACTOR rescales a coils normal vector by a factor.
%
%   coils = SETCOILLOOPFACTOR(coils, loopFactor) scales the normal vectors
%   of the coils provided in the received coils struct by the factor
%   loopFactor.


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021

function coils = setCoilLoopFactor(coils, loopFactor)

for i = 1:numel(coils)
    curCoilN = norm(coils(i).Normal);
    coils(i).Normal = (coils(i).Normal./curCoilN).*loopFactor;
end


end

