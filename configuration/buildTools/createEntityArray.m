%CREATEENTITYARRAY creates an array of entities.
%
%   entities = CREATEENTITYARRAY(startPos, endPos, normal, numOfEntities)
%   creates an array of entities distributed between startPos and endPos
%   equidistantly. Note the outside entities are placed at startPos and
%   endPos. The normal of the vectors is given by the argument normal. If
%   numOfEntities is a single integer, the entities are distributed on the
%   straight line between startPos and endPos. If numOfEntities is a three
%   element vector, the enities are distributed coordinatewise. We create a
%   cube in Cartesian directions between the points startPos and endPos.
%   This cube is filled with entities equidistantly. The number of elements
%   per dimension is given by the entries if numOfEntities.
%
%   entities = CREATEENTITYARRAY(..., type) creates an array of entities as
%   usual. If type is set to 'boundary' or 'b' the distribution of the
%   entities is moved in between startPos and endPos. This means the
%   outside entities have also an equidistant distance to the provided
%   points startPos and endPos.
%
%
%   just run:
%       CREATEENTITYARRAY
%
%   to see a minimal example.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function entities = createEntityArray(startPos, endPos, normal, numOfEntities, type)

%% run example?
if nargin == 0
    entities = runExample;
    return;
end
if nargin < 5 || isempty(type)
    type = [];
end

entities = struct("Position",[],"Normal",[]);


%% function
if numel(numOfEntities)==1
    if strcmp(type,'boundary') || strcmp(type,'b')
        auxPosX = linspace(startPos(1), endPos(1), 2*numOfEntities+1);
        auxPosX = auxPosX(2:2:end);
        auxPosY = linspace(startPos(2), endPos(2), 2*numOfEntities+1);
        auxPosY = auxPosY(2:2:end);
        auxPosZ = linspace(startPos(3), endPos(3), 2*numOfEntities+1);
        auxPosZ = auxPosZ(2:2:end);
    else
        auxPosX = linspace(startPos(1), endPos(1), numOfEntities);
        auxPosY = linspace(startPos(2), endPos(2), numOfEntities);
        auxPosZ = linspace(startPos(3), endPos(3), numOfEntities);
    end
    for k = 1:numOfEntities
        entities(k).Position    = [auxPosX(k),auxPosY(k),auxPosZ(k)];
        entities(k).Normal      = normal;
    end
    
    
elseif numel(numOfEntities)==3
    if strcmp(type,'boundary') || strcmp(type,'b')
        auxPosX = linspace(startPos(1), endPos(1), 2*numOfEntities(1)+1);
        auxPosX = auxPosX(2:2:end);
        auxPosY = linspace(startPos(2), endPos(2), 2*numOfEntities(2)+1);
        auxPosY = auxPosY(2:2:end);
        auxPosZ = linspace(startPos(3), endPos(3), 2*numOfEntities(3)+1);
        auxPosZ = auxPosZ(2:2:end);
    else
        auxPosX = linspace(startPos(1), endPos(1), numOfEntities(1));
        auxPosY = linspace(startPos(2), endPos(2), numOfEntities(2));
        auxPosZ = linspace(startPos(3), endPos(3), numOfEntities(3));
    end
    
    [auxPosYY,auxPosXX,auxPosZZ]    = meshgrid(auxPosY,auxPosX,auxPosZ);
    
    for k = 1:prod(numOfEntities)
        entities(k).Position        = [auxPosXX(k),auxPosYY(k),auxPosZZ(k)];
        entities(k).Normal          = normal;
    end
else
    error('numOfEntities needs to be an integer or an 3 element vector.');
end


%% example
    function entities = runExample()
        
        startPos        = [5.5, 4.5, 1];
        endPos          = [-5.5, -4.5, 0];
        normal          = [-1, 0, 0];
        numOfEntities   = [1 4 2];
        
        entities        = createEntityArray(startPos, endPos, normal, numOfEntities);
        
    end


end

