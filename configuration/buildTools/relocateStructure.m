%RELOCATESTRUCTURE moves a given 3D structure to a new position and
%orientation
%
%   out = RELOCATESTRUCTURE(structure, nStructure, nOut) rotates a 3D point
%   cloud structure. The rotation is defined by the angle between normal
%   nStructure and nOut.
%
%   out = RELOCATESTRUCTURE(..., shift) additionally moves the structure by
%   the vector shift.
%
%   just run:
%       RELOCATESTRUCTURE
%
%   to see a minimal example.


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function out = relocateStructure(structure, nStructure, nOut, shift)

%% run example?
if nargin == 0
    out = runExample;
    return;
end


%% function
if nargin < 4
    shift = [0,0,0];
end
out = zeros(size(structure));

nStructure = nStructure ./ norm(nStructure);
nOut = nOut ./ norm(nOut);

rotvec  = cross(nOut,nStructure);
s       = norm(rotvec);
c       = dot(nOut,nStructure);


if (s==0)
    if c>0
        rotMat = eye(3);
    elseif c<0
        rotMat = [1 0 0; 0 -1 0; 0 0 -1]; % default rotate around x axis
    else
        error('nOut==[0,0,0] is not valid');
    end
else
    ux = rotvec(1);
    uy = rotvec(2);
    uz = rotvec(3);
    rotMat = eye(3) + [0 -uz uy; uz 0 -ux; -uy ux 0] + ([0 -uz uy; uz 0 -ux; -uy ux 0]^2 * ((1 - c) / (s*s)));
end


for i = 1:size(structure,1)
    out(i,:) = structure(i,:)*rotMat;
end

out = out + repmat(shift,size(out,1),1);


%% example
    function out = runExample()
        
        structure = createCoilLoop;
        nStructure = [0,0,1];
        nOut = rand(1,3)-0.5;
        shift = [0,0,0];
        out = relocateStructure(structure,nStructure,nOut,shift);
        
        plot3(structure(:,1),structure(:,2),structure(:,3));
        hold on;
        plot3(out(:,1),out(:,2),out(:,3));
        plot3([nStructure(1);0],[nStructure(2);0],[nStructure(3);0]);
        plot3([shift(1)+nOut(1);shift(1)],[shift(2)+nOut(2);shift(2)],[shift(3)+nOut(3);shift(3)]);
        axis equal;
        hold off;
        
    end


end

