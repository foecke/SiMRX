%CALIBRATECOIL derives the ratio of idealized and a given template coil.
%
%   ratio = calibrateCoil(coilReference, res) derives a factor ratio, which
%   is used to scale the idealized coil to approximate the given reference
%   coil.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       17-Aug-2021


function ratio = calibrateCoil(coilReference, res, distance)

    if nargin < 2
        res = [50,50,50];
    end
    if nargin < 3
        distance = .2;
    end

    roi.x = [-.05, .05];
    roi.y = [-.05, .05];
    roi.z = [-.05, .05];

    coilIdeal.Position  = [mean(roi.x), mean(roi.y), roi.z(2)+distance];
    coilIdeal.Normal    = [0, 0, -1];
        
    coilRef = setCoilLoopFactor(coilIdeal, 1);
    coilRef = parseCoils(coilRef,coilReference);


    %% derive magnetic fields
    voxelGrid               = createVoxelGrid(roi, res);
    magnFieldsIdeal         = createExcitationFields(coilIdeal, voxelGrid);
    magnFieldsReference     = createExcitationFields(coilRef, voxelGrid);

    fieldRatio              = sqrt(sum(magnFieldsReference{1}.^2,2))./sqrt(sum(magnFieldsIdeal{1}.^2,2));
    ratio                   = mean(fieldRatio(:));

    
end

