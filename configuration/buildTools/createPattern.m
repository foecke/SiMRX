%CREATEPATTERN creates a current or measurement pattern.
%
%   config = CREATEPATTERN(entitiesActive, patternType)
%   creates a pattern for coils or sensors of type patternType. The number
%   of entities available is given by numEntities. Then a table with
%   numEntities columns is created, where each entry represents the
%   activation current for the respective coil. All entities in the same
%   row are activated simultaniously.
%
%       'patternType'   defines the type of the created pattern. Each
%                       pattern has additional optional parameter to
%                       specify the required activation. The following
%                       types are available:
%                       'simultaneous' - all entities are activated
%                       'sequential' - a sequence of subsequently
%                           activated entities is created.
%                       'random' - random amount of entities is selected.
%
%	config = CREATEPATTERN(..., varargin) allows optional settings,
%	the first are general options for all pattern types:
%       'current'       integer or interval of currents. if interval, the
%                       the current is picked from the interval with a
%                       normal distribution independently for each entity.
%                       Default: 1
%       'entityList'    defines a list of entities that are used for the
%                       pattern. If set only the specified entities are used.
%                       Default: entities defined by entitiesActive
%       'times'         integer: repeats the process of pattern creation
%                       given amount of times. Default: 1
%
%   The following optional settings are for 'random' only:
%       'p'             integer with 0 < 0.5 <= 1. Defines the probability
%                       that one of the entities is active.
%       'minElements'   defines the minimum number of entities used.
%                       Default: 1
%       'maxElements'   defines the maximum number of entities used.
%                       Default: numEntities
%       'numElements'   if set, it overrules the values of minElements and
%                       maxElements. Then it defines the exact number of
%                       entities active per pattern. Default: [].
%
%   just run:
%       CREATEPATTERN
%
%   to see a minimal example.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function pattern = createPattern(entitiesActive, patternType, varargin)

%% run example?
if nargin == 0
    pattern = runExample;
    return;
end


%% parse default varargins
numEntities = numel(entitiesActive); %temporarily set entity list
entityList  = 1:numEntities;

ip = inputParser;
ip.KeepUnmatched = true;

ip.addParameter('current', 1);
ip.addParameter('entityList', entitiesActive);
ip.addParameter('times', 1);
% for random:
ip.addParameter('p',0.5, @(x) x>0&&x<=1);
ip.addParameter('minElements',1);
ip.addParameter('maxElements',numEntities);
ip.addParameter('numElements',[]);


ip.parse(varargin{:});
current     = ip.Results.current;
% update temp entity list
entityInd   = ismember(entitiesActive,ip.Results.entityList);
entityList  = entityList(entityInd);
times       = ip.Results.times;
p           = ip.Results.p;
minElements = ip.Results.minElements;
maxElements = ip.Results.maxElements;
numElements = ip.Results.numElements;

patternCell = cell(1, times);

for i = 1:times
    %% function code starts here
    switch patternType
        case 'simultaneous' %all chosen entities simultaniously
            out = zeros(1,numEntities);
            out(1,entityList) = out(1,entityList)+1;
            
            
        case 'sequential' %all chosen entities sequentially
            out = zeros(length(entityList),numEntities);
            for k = 1:numel(entityList)
                out(k,entityList(k))=out(k,entityList(k))+1;
            end
            
            
        case 'random' %pick random entity
            if isempty(entityList)
                error('no entities selected with current settings');
            end
            
            if p == 1 && maxElements < numEntities
                error('p=1 with a desired amount of elements is not supported\n');
            end
            
            if minElements == maxElements
                numElements = min(maxElements, numEntities);
            end
            
            if ~isempty(numElements)
                out = zeros(1,numEntities);
                out(entityList) = randperm(numel(entityList)) <= numElements;
            else
                outt = zeros(1,numEntities);
                outt(entityList) = 1;
                out = outt & (rand(1, numEntities)-p)<0;
                while ~(minElements<=nnz(out) && nnz(out)<=maxElements)
                    out = outt & (rand(1, numEntities)-p)<0;
                end
            end
    end
    
    if numel(current)>1
        patternCell{i} = out.*(rand(1, numEntities)*(current(end)-current(1)) + current(1));
    else
        patternCell{i} = out.*current(1);
    end
    
end

pattern = cell2mat(patternCell');


%% example
    function pattern = runExample()
        
        entitiesActive = 3:2:15;
        pattern = [];
        pattern = [pattern; createPattern(entitiesActive,'simultaneous')];
        pattern = [pattern; createPattern(entitiesActive,'sequential', 'entityList', [3,7:12])];
        pattern = [pattern; createPattern(entitiesActive,'random', 'p', 0.5, 'current', [0.5,1])];
        pattern = [pattern; createPattern(entitiesActive,'random', 'entityList', 5, 'current', [0,1])];
        pattern = [pattern; createPattern(entitiesActive,'random', 'entityList', [3,7:12],'current',0.5, 'numElements', 2, 'times', 5)];
        fid = gcf;
        imagesc(pattern);
        fid.CurrentAxes.XLabel.String = 'Entity';
        xticklabels(fid.CurrentAxes,entitiesActive);
        fid.CurrentAxes.YLabel.String = 'Pattern';
        
    end


end

