%CLEANUPPATTERNS removes empty patterns from  provided current and
%   measurement patterns.
%
%   config = CLEANUPPATTERNS(config) analyses the config.currentPattern and
%   config.measurementPattern. It removes all patterns, that does have at
%   least one entry in the current pattern AND one entry in the measurement
%   pattern. Fuilly empty patterns or such that only entries in one or the
%	other are deletes as well.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function config = cleanupPatterns(config)

indCurrent  = any(config.currentPattern, 2);
indMeas     = any(config.measurementPattern, 2);
ind         = and(indCurrent, indMeas);
config.currentPattern       = config.currentPattern(ind, :);
config.measurementPattern   = config.measurementPattern(ind, :);


end

