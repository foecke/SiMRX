%   README.m can be executed to run all scripts to fully compile this
%   setup. Then examples/exampleA.m shows an example how to use this setup.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


if(~isdeployed)
    cd(fileparts(which(mfilename)));
end


%% load SiMRX toolbox
run('../../initSiMRX.m'); % load SiMRX path relative to this example script


%%
decoratedBox('Run scripts for setup ''default2D''');

% First create the setup file 'default.mrxsetup'
run('scripts/createSetup.m');

% Second create a config file that is suitable for 'default.mrxsetup'
run('scripts/createConfigs.m');

decoratedBox('setup ''default2D'' done');

