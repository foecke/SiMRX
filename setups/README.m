% This folder contains three setups 'default2D', 'default3D' and
% 'realistic3D'.
% Since handling a MRXI setup is very modular, the authors recommend a
% specific folder structure. Please read the tech report for more
% information on the folder structure.
%
% This file can be executed to compile all three example setups. The
% provided example in the folder 'examples/' then show how to use the
% different types of setups.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


if(~isdeployed)
    cd(fileparts(which(mfilename)));
end


%% load SiMRX toolbox
run('../initSiMRX.m'); % load SiMRX path relative to this script


%% run default2D init script
run('default2D/README.m');


%% run default3D init script
run('default3D/README.m');


%% run default2D init script
run('realistic3D/README.m');

