% This script creates SiMRX setup called realistic3D from raw files.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


tempPath = pwd;

if(~isdeployed)
    cd([fileparts(which(mfilename)), '/..']);
end


%%
cla;
fid = figure('Visible', 'off');


%% set a filesetID just for convenience
filesetID           = 1;


%% load external setup
% reset setup struct and set dimension
setup               = [];
setup.info.name     = 'realistic3D';
setup.info.variant  = 'default';

setup.dim           = 3;

% load and store region of interest
voxelGrid           = load('rawData/voxelGrid.dat');
setup.roi           = getROI(voxelGrid);

% load and store coil positions
coilPosition        = load('rawData/coilGrid.dat');

for i = 1:size(coilPosition,1)
    setup.coils(i).Position	= coilPosition(i,:);
    % as of now, the normal of coils are not stored in the textfile. So for
    % the realistic dataset all coils are "pointing" upwards.
    % In other ascii based datasets this might be different!
    setup.coils(i).Normal 	= [0,0,1];
end

% load and apply coil template
coilTemplate        = load('rawData/coilTemplate.dat');
setup.coils         = parseCoils(setup.coils,coilTemplate);

% load sensors
sensors             = load('rawData/sensors.dat');

% % remove dead sensors:
% % check if the given sensors has been active during measurement .
% % Otherwise the sensor is deleted from the sensor list.
relax               = load(sprintf('rawData/dataset.%02i.relax.dat', filesetID));
% id2rm               = setdiff(unique(sensors(:,7)), unique(relax(:,2)));
% id2rmIndex          = ~ismember(sensors(:,7),id2rm);
% sensors             = sensors(id2rmIndex,:);
% % cleaning done

for i = 1:size(sensors,1)
    setup.sensors(i).Position	= sensors(i,1:3);
    setup.sensors(i).Normal 	= sensors(i,4:6);
    setup.sensors(i).SensorID 	= sensors(i,7);     %unused
    setup.sensors(i).ChannelID 	= sensors(i,8);     %unused
    setup.sensors(i).GroupID 	= sensors(i,9);     %unused
end

setup.coilGroups = {};
setup.sensorGroups = {};

saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);


%%
cd(tempPath);

