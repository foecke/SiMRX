% This script loads a measurement from a raw file.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


if(~isdeployed)
    cd([fileparts(which(mfilename)), '/..']);
end


%% set dataset
filesetID           = 1;
res                 = [10,10,5];


%% load setup
setup = loadSetup('default'); %loads default.mrxsetup
visualizeMRX(setup);


%% cleaup system depending on measured data
relax               = load(sprintf('rawData/dataset.%02i.relax.dat', filesetID));

% remove dead sensors:
% check if the given sensors has been active during measurement .
% Otherwise the sensor is deleted from the sensor list.
id2rm               = setdiff(unique([setup.sensors.SensorID]'), unique(relax(:,2)));
id2rmIndex          = ~ismember([setup.sensors.SensorID]',id2rm);
setup.sensors       = setup.sensors(id2rmIndex);


%% set config
config = loadConfig(sprintf('configs/all/singleSequential.dataset.%02i',filesetID));


%% simulate setup with given config
A	= createMRXMatrix(setup, config, res);

% rescale matrix to fit to real system
mu0 = pi * 4e-7;
A   = A./mu0;

gMeas = relax(:,1);

