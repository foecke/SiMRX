% This script creates SiMRX setup and config raw files as plain text files.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


if(~isdeployed)
    cd([fileparts(which(mfilename)), '/..']);
end

%% create setup
setup               = [];
setup.info.name     = 'realistic3D';

setup.dim           = 3;

setup.roi.x         = [-.05 , .05];
setup.roi.y         = [-.05 , .05];
setup.roi.z         = [-.025, .025];

% add coils
% below
setup.coils         = [];
setup.coils         = [setup.coils, createEntityArray([-.0525,-.0525,-.030], [.0175, .0175,-.030], [0,0,1], [2,2,1])];
setup.coils         = [setup.coils, createEntityArray([-.0175,-.0175,-.030], [.0525, .0525,-.030], [0,0,1], [2,2,1])];
% above
setup.coils         = [setup.coils, createEntityArray([-.0525,-.0175, .030], [.0175, .0525, .030], [0,0,1], [2,2,1])];
setup.coils         = [setup.coils, createEntityArray([-.0175, .0175, .030], [.0525,-.0525, .030], [0,0,1], [2,2,1])];

setup.coilGroups    = {1:4, 5:8, 9:12, 13:16};

% add sensors
setup.sensors       = [];
setup.sensors       = [setup.sensors, createEntityArray([-.050,-.050,.050], [.040,.040,.050], [ 1, 0, 0], [5,5,1])];
setup.sensors       = [setup.sensors, createEntityArray([-.040,-.040,.050], [.050,.050,.050], [-1, 0, 0], [5,5,1])];
setup.sensors       = [setup.sensors, createEntityArray([-.050,-.040,.050], [.040,.050,.050], [ 0,-1, 0], [5,5,1])];
setup.sensors       = [setup.sensors, createEntityArray([-.040,-.050,.050], [.050,.040,.050], [ 0, 1, 0], [5,5,1])];
setup.sensors       = [setup.sensors, createEntityArray([-.055,-.055,.045], [.055,.055,.045], [ 0, 0, 1], [5,5,1])];
setup.sensors       = [setup.sensors, createEntityArray([-.045,-.045,.055], [.045,.045,.055], [ 0, 0,-1], [5,5,1])];
setup.sensorGroups  = {1:25, 26:50, 51:75, 76:100, 101:125, 126:150};

setup.info.variant          = 'coil20w1000Spiral23';
coil20w1000Spiral23         = createCoilLoop(.023,20,1000,'spiral');
setup.coils                 = parseCoils(setup.coils,coil20w1000Spiral23);


%% create config
config                      = [];
config.info.name            = 'all';
config.coilsActive          = unique([setup.coilGroups{:}]);
config.sensorsActive        = unique([setup.sensorGroups{:}]);

config.info.variant         = 'singleSequential';
config.currentPattern       = createPattern(config.coilsActive, 'sequential', 'current', 0.85);
config.measurementPattern   = createPattern(config.sensorsActive, 'simultaneous', 'times', size(config.currentPattern, 1));

res                         = [10, 10, 5];


%% save setup to raw files
if ~exist('rawData', 'dir')
    mkdir('rawData');
end

% save coil grid
numOfCoils          = numel(setup.coils);
coils               = cell2mat({setup.coils.Position}');
save('rawData/coilGrid.dat', 'coils', '-ascii');

% save coil template
save('rawData/coilTemplate.dat', 'coil20w1000Spiral23', '-ascii');

% save sensors
numOfSensors        = numel(setup.sensors);
sensorSensorID      = 1:numOfSensors;
sensorChannelID     = zeros([numOfSensors, 1]);
sensorGroupID       = zeros([numOfSensors, 1]);
sensors             = [cell2mat([{setup.sensors.Position}',{setup.sensors.Normal}']),sensorSensorID',sensorChannelID,sensorGroupID];
save('rawData/sensors.dat', 'sensors', '-ascii');

% save voxelgrid
voxelGrid = createVoxelGrid(setup.roi, res);
save('rawData/voxelGrid.dat', 'voxelGrid', '-ascii');


%% create Systemmatrix
mu0                 = pi * 4e-7;
A                   = createMRXMatrix(setup, config, res);
A                   = A./mu0;


%%
for filesetID = 1:5
    
    %% create phantom
    ChiMNP              = 0.0005; % set susceptibility of the MNP
    MNPmg               = 100; %Magnetic Nanoparticles in mg in the region of interest
    
    f                   = createPhantom(res, 'P', 'options', struct('XMNP',MNPmg * (ChiMNP * 1e15), 'layer', filesetID));
    f                   = f(:).*mu0; %undo the rescale in the real data
    
    
    %% create measurement
    g                   = A * f;
    
    
    %% currents and measurements to file
    % save currents - FOR SUBSEQUENT ACTIVATIONS ONLY
    currents            = diag(config.currentPattern);
    save(sprintf('rawData/dataset.%02i.currents.dat', filesetID), 'currents', '-ascii');
    
    % save relaxation data
    numOfMeas           = numel(g);
    relaxSensorID       = repmat(1:numOfSensors, [1,numOfCoils]);
    relaxChannelID      = zeros([numOfMeas, 1]);
    relaxGroupID        = zeros([numOfMeas, 1]);
    relaxCoilID         = repmat(1:numOfCoils, [numOfSensors,1]);
    relax               = [g, relaxSensorID', relaxChannelID, relaxGroupID, relaxCoilID(:)];
    save(sprintf('rawData/dataset.%02i.relax.dat', filesetID), 'relax', '-ascii');
    
end
