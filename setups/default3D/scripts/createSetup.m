% This script creates SiMRX setup called default3D.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


tempPath = pwd;

if(~isdeployed)
    cd([fileparts(which(mfilename)), '/..']);
end


%%
cla;
fid = figure('Visible', 'off');


%% create a new 3D setup
setup               = [];
setup.info.name     = 'default3D';

setup.dim           = 3;

setup.roi.x         = [-.05, .05];
setup.roi.y         = [-.05, .05];
setup.roi.z         = [-.05, .05];

setup.coils         = [];
setup.coils         = [setup.coils, createEntityArray([-.055,-.035,-.035], [-.055, .035, .035], [ 1, 0, 0], [ 1, 3,3])];
setup.coils         = [setup.coils, createEntityArray([ .055,-.035,-.035], [ .055, .035, .035], [-1, 0, 0], [ 1, 3,3])];
setup.coils         = [setup.coils, createEntityArray([-.035,-.055,-.035], [ .035,-.055, .035], [ 0, 1, 0], [ 3, 1,3])];
setup.coils         = [setup.coils, createEntityArray([-.035, .055,-.035], [ .035, .055, .035], [ 0,-1, 0], [ 3, 1,3])];
setup.coils         = [setup.coils, createEntityArray([-.035,-.035,-.055], [ .035, .035,-.055], [ 0, 0, 1], [ 3, 3,1])];
setup.coils         = [setup.coils, createEntityArray([-.035,-.035, .055], [ .035, .035, .055], [ 0, 0,-1], [ 3, 3,1])];

setup.coilGroups    = {1:9, 10:18, 19:27, 28:36, 37:45, 46:54};

setup.sensors       = [];
setup.sensors       = [setup.sensors, createEntityArray([-.05,-.05, .095], [ .05, .05, .095], [ 0, 0,-1], [10,10,1])];

setup.sensorGroups  = {1:100};


%% save setup variants
setup.info.variant          = 'idealizedUnscaled';
setup.coils                 = setCoilLoopFactor(setup.coils, 1);
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);
setup.info.variant          = 'idealized';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);

coil_r0_015_w100_s1000_loop = createCoilLoop(.015,100,1000,'loop');
coil_r0_015_w100_s1000_loop_factor = calibrateCoil(coil_r0_015_w100_s1000_loop);
setup.info.variant          = 'coil_r0_015_w100_s1000_loop_idealized';
setup.coils                 = setCoilLoopFactor(setup.coils, coil_r0_015_w100_s1000_loop_factor);
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);

setup.info.variant          = 'coil_r0_015_w100_s1000_loop';
setup.coils                 = setCoilLoopFactor(setup.coils, 1);
setup.coils                 = parseCoils(setup.coils,coil_r0_015_w100_s1000_loop);
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);
setup.info.variant          = 'default';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);


%%
cd(tempPath);

