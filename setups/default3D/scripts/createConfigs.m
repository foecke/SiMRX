% This script creates a SiMRX config for the default3D setup.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


tempPath = pwd;

if(~isdeployed)
    cd([fileparts(which(mfilename)), '/..']);
end


%% load setup
setup = loadSetup('default');


%% create default config
config                      = [];
config.info.name            = 'all';
config.coilsActive          = unique([setup.coilGroups{:}]);
config.sensorsActive        = unique([setup.sensorGroups{:}]);

config.info.variant         = 'singleSequential';
config.currentPattern       = [];
config.measurementPattern   = [];
currentPatternTemp          = createPattern(config.coilsActive, 'sequential', 'current', 0.8249);
measurementPatternTemp      = createPattern(config.sensorsActive, 'simultaneous', 'times', size(currentPatternTemp, 1));
config.currentPattern       = [config.currentPattern; currentPatternTemp];
config.measurementPattern   = [config.measurementPattern; measurementPatternTemp];
if checkCompatibility(setup, config) % check compatibility with setup and save config
    saveConfig(sprintf('configs/%s/%s', config.info.name, config.info.variant), config, 'ff', true);
end

config.info.variant         = 'random10times3coils';
config.currentPattern       = [];
config.measurementPattern   = [];
currentPatternTemp          = createPattern(config.coilsActive, 'random', 'current', [0.8,0.85], 'numElements', 3, 'times', 10);
measurementPatternTemp      = createPattern(config.sensorsActive, 'simultaneous', 'times', size(currentPatternTemp, 1));
config.currentPattern       = [config.currentPattern; currentPatternTemp];
config.measurementPattern   = [config.measurementPattern; measurementPatternTemp];
if checkCompatibility(setup, config) % check compatibility with setup and save config
    saveConfig(sprintf('configs/%s/%s', config.info.name, config.info.variant), config, 'ff', true);
end

cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 1, 'export', sprintf('configs/%s/visFull', config.info.name));
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 0, 'export', sprintf('configs/%s/visCoils', config.info.name));
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 0, 'showSensors', 1, 'export', sprintf('configs/%s/visSensors', config.info.name));


%%
cd(tempPath);
