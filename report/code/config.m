%% create a new 2D config
config                      = [];

config.info.name            = 'all';
config.coilsActive          = unique([setup.coilGroups{:}]);
config.sensorsActive        = unique([setup.sensorGroups{:}]);

config.info.variant         = 'singleSequential';
config.currentPattern       = createPattern(config.coilsActive, 'sequential', ...
                                'current', 0.8);
config.measurementPattern   = createPattern(config.sensorsActive, 'simultaneous', ...
                                'times', size(config.currentPattern, 1));

%% check compatibility with setup and save config
if checkCompatibility(setup, config)
    saveConfig(sprintf('configs/%s/%s', config.info.name, config.info.variant), config);
end

