%% create a new 2D setup
setup               = [];
setup.info.name     = '2Dsetup';

setup.dim           = 2;

setup.roi.x         = [-.05, .05];
setup.roi.y         = [-.05, .05];
setup.roi.z         = [   0,   0];

coils               = [];
coils = [coils, createEntityArray([ .055,.045,0], [ .055,-.045,0], [-1,0,0], [1,10,1])];
coils = [coils, createEntityArray([-.055,.045,0], [-.055,-.045,0], [ 1,0,0], [1,10,1])];
coils = [coils, createEntityArray([.045, .055,0], [-.045, .055,0], [0,-1,0], [10,1,1])];
coils = [coils, createEntityArray([.045,-.055,0], [-.045,-.055,0], [0, 1,0], [10,1,1])];
setup.coils         = coils;
setup.coilGroups    = {1:10, 11:20, 21:30, 31:40};

sensors             = [];
sensors = [sensors, createEntityArray([.050,.060,0], [-.050,.060,0], [-1,-1,0], 10)];
sensors = [sensors, createEntityArray([.050,.060,0], [-.050,.060,0], [ 1,-1,0], 10)];
setup.sensors       = sensors;
setup.sensorGroups  = {1:10, 11:20};

visualizeMRX(setup);


%% validate and save setup
setup.info.variant  = 'default';
if isSetupValid(setup)
    saveSetup(setup.info.variant, setup);
end

