%!TEX root = ../main.tex
\section{Introduction}
Many new and experimental treatment methods in medical applications use magnetic nanoparticles as a contrast agent.
These particles allow for multiple different approaches (\cite{hiergeist1999ApplicationMagnetiteFerrofluids, alexiou2011CancerTherapyDrug}), however for the named methods the exact knowledge about the particle distribution is crucial.
Here, Magnetorelaxometry (MRX) can be used to determine the amount of particles in a region of interest as shown in \cite{baumgarten2008MagneticNanoparticleImaging, liebl2014QuantitativeImagingMagnetic}.
Based on this approach an imaging technique called Magnetorelaxometry Imaging (MRXI) has been proposed.

The \simrx toolbox provides a set of tools to model and simulate such an MRX system.
It is based on the mathematical model developed in \cite{focke2018InverseProblemMagnetorelaxometry}.

\subsection{Model}
This following section (including notation) is part of \cite{focke2018InverseProblemMagnetorelaxometry}, which, for interested readers, provides an in depth look in the analysis of the following operator.
The magnetic field in $w\in\Omega$ induced by a coil $\alpha = (\varphi_\alpha, I_\alpha)$ is given by
\begin{equation}
\mathbf{B}_\alpha^\textbf{coil}\colon\Omega \rightarrow\R^3, \quad w \mapsto\vartheta\int\limits_0^{L_\alpha}\varphi_\alpha^\prime(s)\times\left(\frac{w-\varphi_\alpha(s)}{\left\vert w-\varphi_\alpha(s)\right\vert^3}\right)ds,\label{eq:biotsavart}
\end{equation}
where ${L_\alpha}$ is the length of the coil.
The magnetization of the magnetic nanoparticles (MNP) after the reorientation process in $w$ is described by
\begin{equation}
\mathbf{m}_\alpha\colon \Omega\rightarrow\R^3, \quad w \mapsto\frac{1}3 \mathbf{B}_\alpha^\textbf{coil}(w)c(w),\label{eq:dipolemagnetization}
\end{equation}
where $c$ is the desired particle distribution.
The particle induced magnetic response from particles in $w$ measured by a sensor $\sigma = (\sigma_x, \sigma_n)$ is modeled by
\begin{align}
	\mathbf{B}^\textbf{meas}_\alpha\colon\Omega\times\Sigma&\rightarrow\R\nonumber\\
	\left(w,\sigma\right)&\mapsto\sigma_n\cdot\left(\left(\frac{3\left(\sigma_x-w\right)\otimes\left(\sigma_x-w\right)}{\left\vert\sigma_x-w\right\vert^5}-\frac{\mathbb{I}}{\left\vert\sigma_x-w\right\vert^3}\right)\mathbf{B}_\alpha^\textbf{coil}(w)\right).\label{eq:measmagnetization}
\end{align}
In the end we receive the following forward operator for a coil $\alpha$:
\begin{equation}
\mathbf{K}_\alpha\colon\mathcal{L}^2(\Omega) \rightarrow\mathcal{L}^2(\Sigma), \quad c \mapsto\left[\sigma\mapsto\int\limits_\Omega\mathbf{B}^\textbf{meas}_\alpha(w, \sigma)\ c(w) d^3w\right].\label{eq:fredholmformulation}
\end{equation}


\subsection{Discretization}\label{ss:coilDiscretization}
First we consider the 3D case:
Here the conductor coil $\varphi_\alpha$ is approximated by a set list of segments, with starting points $a_k$ and ending points $b_k$ for the $k$-th segment respectively.
Then the magnetic field in $w\in\Omega$ is \cite{hanson2002CompactExpressionsBiot}:
\begin{align}
	\mathbf{B}_{\alpha,k}^\textbf{coil}\colon\Omega&\rightarrow\R^3\label{eq:discreteActivation}\nonumber\\
	w&\mapsto\vartheta\frac{\vert a_k-w\vert+\vert b_k-w\vert}{\vert a_k-w\vert\vert b_k-w\vert}\frac{(a_k-w)\times(b_k-w)}{\vert a_k-w\vert\vert b_k-w\vert+(a_k-w)\cdot(b_k-w)}.
\end{align}
The MNP response (see equation \eqref{eq:measmagnetization}) still holds in the discrete case.

In the 2D case a coil simplification leads to the usage of variants of \eqref{eq:measmagnetization} for both coil and dipole response (see \cite[section 4.2]{focke2018InverseProblemMagnetorelaxometry}).
This is based on the idea tha -- from a certain distance -- a coil induced magnetic field is structurally indistinguishable to a appropriately strong dipole field.
