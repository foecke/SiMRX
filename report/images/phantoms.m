% %  This script requires the helperFiles toolbox:
% %  https://gitlab.com/foecke/helperFiles
%   
%   addpath('~/matlab/helperFiles/');
%   initHelperFiles;


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       23-Okt-2019


addpath('../../');
initSiMRX();

%%
resolution		= [50,50,15];
cutOff          = 2;
volStack = [];
out = flipXY(createPhantom(resolution, 'F_2', 100, 'nearest'));
volStack{1} = out(:,:,cutOff+1:end-cutOff);
out = flipXY(createPhantom(resolution, 'shepplogan3d', 100, 'nearest'));
volStack{2} = out(:,:,cutOff+1:end-cutOff);
out = flipXY(createPhantom(resolution, 'tumor', 100, 'nearest'));
volStack{3} = out(:,:,cutOff+1:end-cutOff);
out = flipXY(createPhantom(resolution, 'fwhmdots_0.25', 100, 'nearest'));
volStack{4} = out(:,:,cutOff+1:end-cutOff);

showAllSlices(volStack,'clim','self');

exportFigure('phantoms.pdf');

