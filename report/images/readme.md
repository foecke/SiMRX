# README

To create the images in this report use the following scripts in Tex/MATLAB:

* phantoms.m -> phantoms.pdf
* systemmatrix.tex -> systemmatrix.pdf
* vis.m -> vis.pdf
