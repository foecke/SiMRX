% Example A
% =========
%
% The scripts in this folder illustrate how to use the SiMRX toolbox. To
% ensure that these examples work, please run README.m first!
%
% This example loads a predefined setup and config file and simulates the
% corresponding system matrix A


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


close all;
restoredefaultpath;
clearvars;


%%
if(~isdeployed)
    cd(fileparts(which(mfilename)));
end


%% load SiMRX toolbox
addpath('..'); % load SiMRX path relative to this example script
initSiMRX();


%% load setup and config file
setupPath   = '../setups/default2D/default.mrxsetup';
configPath  = '../setups/default2D/configs/all/singleSequential.mrxcfg';
res         = [10, 10, 1];

setup       = loadSetup(setupPath);
config      = loadConfig(configPath);


%% simulate system matrix
A           = createMRXMatrix(setup, config, res);

