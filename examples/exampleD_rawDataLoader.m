% Example D
% =========
%
% The scripts in this folder illustrate how to use the SiMRX toolbox. To
% ensure that these examples work, please run README.m first!
%
% This examples uses the RAWDATALOADER (in the <SiMRX-root>/misc folder).
% The script makes use of the raw export for system matrix and measurement
% processes.
% As input information, the script requires the used setup and config
% name/variant. Then a list of phantoms is created, which are simulated in
% the system. Internally, the system matrices are stored in the raw data
% format, hence, only required parts of the system matrix are simulated and
% loaded otherwise. The measured response for each phantom is also stored,
% so in case a certain measurement for a given setup/config has been
% already performed, this dataset is loaded instead. Consequently, this
% script not only simulates a system, but also manages save files and loads
% previously derived systems from the data source.

%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       18-Oct-2021


close all;
restoredefaultpath;
clearvars;


%%
if(~isdeployed)
    cd(fileparts(which(mfilename)));
end


%% load SiMRX toolbox
addpath('..'); % load SiMRX path relative to this example script
initSiMRX();


%% 

dataIn.setupPath        = '../setups';
dataIn.setupName        = 'default2D'; 
dataIn.setupVariant     = 'default';
dataIn.configName       = 'all';
dataIn.configVariant    = 'singleSequential';  

dataIn.resRecon         = [25,25,1];  

dataIn.phantom(1).shape             = 'tumor';
dataIn.phantom(1).res               = [29,29,1];    
dataIn.phantom(1).options.densityA  = 0.1;   
dataIn.phantom(1).options.densityB  = 0.5;

dataIn.phantom(2).shape             = 'tumor';
dataIn.phantom(2).res               = [29,29,1];    
dataIn.phantom(2).options.densityA  = 0.1;   
dataIn.phantom(2).options.densityB  = 0.1;


dataOut                 = rawDataLoader(dataIn);
% The system matrix is now given in dataOut.A
% The individual measurements for each phantom are given in
% dataOut.measurement respectively.
% For more details on the stored information in the dataOut struct, please
% read the help section for RAWDATALOADER .



