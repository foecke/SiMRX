% Example B
% =========
%
% The scripts in this folder illustrate how to use the SiMRX toolbox. To
% ensure that these examples work, please run README.m first!
%
% This example loads a predefined setup and config file and stores the
% simulated system information in the raw data format. Finally the data is
% loaded and combined to a proper system matrix.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


close all;
restoredefaultpath;
clearvars;


%%
if(~isdeployed)
    cd(fileparts(which(mfilename)));
end


%% load SiMRX toolbox
addpath('..'); % load SiMRX path relative to this example script
initSiMRX();


%% set paths, setup and config
setupFolder     = '../setups';

setupName       = 'default3D';
setupVariant    = 'default';
configName      = 'all';
configVariant   = 'singleSequential';

configPath      = sprintf('%s/%s/configs/%s/%s.mrxcfg', setupFolder, setupName, configName, configVariant);
setupPath       = sprintf('%s/%s/%s.mrxsetup', setupFolder, setupName, setupVariant);
res             = [10,10,5];


%% load setup and config
config          = loadConfig(configPath);
setup           = loadSetup(setupPath);


%% set raw path, export raw files, and simulate system from raw files

rawPath         = sprintf('%s/%s/raw/%s', setupFolder, setupName, setupVariant);

if checkCompatibility(setup, config)
    integrity   = isRawValid(setup, rawPath, res);
    if ~integrity
        rawExportSetup(setup, rawPath, res, config);
    end
    
    % load ARaw from stored files
    ARaw        = rawImportSetup(rawPath, res);
    A           = applyPatterns(ARaw, config);
end

