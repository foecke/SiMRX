% Example C
% =========
%
% The scripts in this folder illustrate how to use the SiMRX toolbox. To
% ensure that these examples work, please run README.m first!
%
% This example loads setup and config from a dataset created from external
% data files. This external data may be created from real experiments.
%
% The script first loads the measured data, removes defective sensors and
% compares the measured data to simulated data. In this case the 'loaded
% data from experiment' are identical to 'simulated data' since the
% 'experimental data' has been created with this toolbox as well. However
% this script illiustrates the workflow that is required to handle real
% experimenal data.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


close all;
restoredefaultpath;
clearvars;


%%
if(~isdeployed)
    cd(fileparts(which(mfilename)));
end


%% load SiMRX toolbox
addpath('..'); % load SiMRX path relative to this example script
initSiMRX();


%% set dataset
filesetID  	= 1;


%%
setupPath   = '../setups/realistic3D/default.mrxsetup';
configPath  = sprintf('../setups/realistic3D/configs/dataset.%02i/singleSequential.mrxcfg', filesetID);
rawPath     = '../setups/realistic3D/raw/default';
rawDataPath = '../setups/realistic3D/rawData';
res         = [10,10,5];

setup       = loadSetup(setupPath);
config      = loadConfig(configPath);

% load measured dataset
relax       = load(sprintf('%s/dataset.%02i.relax.dat', rawDataPath, filesetID));


%% cleaup system depending on measured data
% remove dead sensors:
% check if the given sensors has been active during measurement .
% Otherwise the sensor is deleted from the sensor list.
id2rm        	= setdiff(unique([setup.sensors.SensorID]'), unique(relax(:,2)));
id2rmIndex  	= ~ismember([setup.sensors.SensorID]',id2rm);
setup.sensors 	= setup.sensors(id2rmIndex);


%% simulate setup with given config
A	= createMRXMatrix(setup, config, res);

% rescale matrix to fit to real system
mu0 = pi * 4e-7;
A   = A./mu0;

gMeas = relax(:,1);


%% simulate system measurement

phantomShape 	= 'P';        % same phantom as in measured data
MNPmg           = 100;          %Magnetic Nanoparticles in mg in the region of interest
ChiMNP          = 0.0005;      % susceptibility of the MNP

f               = createPhantom(res, phantomShape, 'options', struct('XMNP',MNPmg * (ChiMNP * 1e15), 'layer', filesetID));
f               = f(:).*mu0;    %undo the rescale in the real data
gSim            = A * f;


%%
figure;
hold on;
plot(gSim);
plot(gMeas);

