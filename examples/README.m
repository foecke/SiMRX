% This folder contains simple example scripts, that show how the SiMRX
% toolbox is used. Please run this README script first, to properly create
% the provided example setups.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


if(~isdeployed)
    cd(fileparts(which(mfilename)));
end


%% load SiMRX toolbox
run('../initSiMRX.m'); % load SiMRX path relative to this script


%% run all setup scripts
run('../setups/README.m');

