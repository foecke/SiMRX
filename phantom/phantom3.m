%PHANTOM3 creates analytical 3D phantoms
%   p = PHANTOM3(def, n) generates a phantom p with spatial resolution n^3
%   and consist of a set of multiple ellpises (representing a
%   simplification of a brain).
%
%   Input Variable def may be a string or char vector the specify the
%   phantom type. The following phantoms are available:
%       'Shepp-Logan'           default Shepp-Logan phantom
%       'Modified Shepp-Logan'  Shepp-Logan with improved contrast
%
%   n is a scalar that defines the size of the produced phantom. If n is
%   not specified, n defaults to 50.
%
%   p = phantom3(e, n) generates a phantom p, where e specifies a matrix.
%   Each row defines an ellipse in the image. E has 10 columns, with each
%   column containing parameter for the ellipses:
%   	Column 01:  A       the additive intensity value of the ellipse
%       Column 02:  a       the length of the x semi-axis of the ellipse
%       Column 03:  b       the length of the y semi-axis of the ellipse
%       Column 04:  c       the length of the z semi-axis of the ellipse
%       Column 05:  x0      the x-coordinate of the center of the ellipse
%       Column 06:  y0      the y-coordinate of the center of the ellipse
%       Column 07:  z0      the z-coordinate of the center of the ellipse
%       Column 08:  alpha   the angle between the x axis and the N axis
%       Column 09:  beta    the angle between the z axis and the Z axis
%       Column 10:  gamma   the angle between the N axis and the X axis
%   The angles are choosen according to the x-convention (Euler angles:
%   (z, x′, z″) or (Z_1, X_2, Z_3)).
%
%   Example: (run phantom3() without any argument to show example)
%
%       e =    [1.0000  .6900  .9200  .8100      0      0      0      0      0      0
%               -.8000  .6624  .8740  .7800      0 -.0184      0      0      0      0
%               -.2000  .1100  .3100  .2200  .2200      0      0 -18.00      0  10.00];
%       out = phantom3(e,20);
%


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


%   This code is using the code of phantom.m as a reference
%   Copyright 1993-2017 The MathWorks, Inc.


function [p,e]=phantom3(s, n)

%% run example?
if nargin < 1
    p = runExample;
    return;
end


%% parse inputs
if nargin < 2
    n = 50;
end

if ischar(s)
    e = selectTemplate(s);
elseif size(s, 2)==10
    e = s;
else
    error('input not valid');
end


%% calc ellipses and plot phantom
xax     = ((0:(n-1))-(n-1)/2) / ((n-1)/2);
[x,y,z] = meshgrid(xax,xax,xax);
vol     = [x(:)'; y(:)'; z(:)'];

p       = zeros(n,n,n);

for k = 1:size(e,1)
    
    A       = e(k,1);             % Amplitude change for this ellipse
    asq     = e(k,2)^2;           % a^2
    bsq     = e(k,3)^2;           % b^2
    csq     = e(k,4)^2;           % c^2
    x0      = e(k,5);             % x offset
    y0      = e(k,6);             % y offset
    z0      = e(k,7);             % z offset
    alpha   = e(k,8)*pi/180;      % rotation angle in radians
    beta    = e(k,9)*pi/180;      % rotation angle in radians
    gamma   = e(k,10)*pi/180;     % rotation angle in radians
    
    cosalpha    = cos(alpha);
    sinalpha    = sin(alpha);
    cosbeta     = cos(beta);
    sinbeta     = sin(beta);
    cosgamma    = cos(gamma);
    singamma    = sin(gamma);
    
    % Euler angles: (z, x′, z″) or (Z_1, X_2, Z_3)
    M = [ cosalpha*cosgamma-sinalpha*cosbeta*singamma   sinalpha*cosgamma+cosalpha*cosbeta*singamma  sinbeta*singamma;
        -cosalpha*singamma-sinalpha*cosbeta*cosgamma  -sinalpha*singamma+cosalpha*cosbeta*cosgamma  sinbeta*cosgamma;
        sinalpha*sinbeta                             -cosalpha*sinbeta           cosbeta];
    volPhantom   = M*vol;
    
    idx     = find((volPhantom(1,:)-x0).^2./asq + (volPhantom(2,:)-y0).^2./bsq + (volPhantom(3,:)-z0).^2./csq <= 1);
    p(idx)  = p(idx) + A;
    
end


%% define templates
    function e = selectTemplate(pName)
        switch pName
            
            case 'shepp-logan'
                %   default head phantom taken from AK Jain, 439
                %   A      a      b      c     x0     y0     z0    phi  theta    psi
                e =    [
                    1.0000  .6900  .9200  .8100      0      0      0      0      0      0
                    -.9800  .6624  .8740  .7800      0 -.0184      0      0      0      0
                    -.0200  .1100  .3100  .2200  .2200      0      0 -18.00      0  10.00
                    -.0200  .1600  .4100  .2800 -.2200      0      0  18.00      0  10.00
                    .0100  .2100  .2500  .4100      0  .3500 -.1500      0      0      0
                    .0100  .0460  .0460  .0500      0     .1  .2500      0      0      0
                    .0100  .0460  .0460  .0500      0    -.1  .2500      0      0      0
                    .0100  .0460  .0230  .0500 -.0800 -.6050      0      0      0      0
                    .0100  .0230  .0230  .0200      0 -.6060      0      0      0      0
                    .0100  .0230  .0460  .0200  .0600 -.6050      0      0      0      0];
                
                
            case 'modified-shepp-logan'
                %   Taken from Toft, 199-200.
                %   A      a      b      c     x0     y0     z0    phi  theta    psi
                e =    [
                    1.0000  .6900  .9200  .8100      0      0      0      0      0      0
                    -.8000  .6624  .8740  .7800      0 -.0184      0      0      0      0
                    -.2000  .1100  .3100  .2200  .2200      0      0 -18.00      0  10.00
                    -.2000  .1600  .4100  .2800 -.2200      0      0  18.00      0  10.00
                    .1000  .2100  .2500  .4100      0  .3500 -.1500      0      0      0
                    .1000  .0460  .0460  .0500      0     .1  .2500      0      0      0
                    .1000  .0460  .0460  .0500      0    -.1  .2500      0      0      0
                    .1000  .0460  .0230  .0500 -.0800 -.6050      0      0      0      0
                    .1000  .0230  .0230  .0200      0 -.6060      0      0      0      0
                    .1000  .0230  .0460  .0200  .0600 -.6050      0      0      0      0];
                
                
        end
    end


%% example
    function out = runExample()
        
        e =    [
            1.0000  .6900  .9200  .8100      0      0      0      0      0      0
            -.8000  .6624  .8740  .7800      0 -.0184      0      0      0      0
            -.2000  .1100  .3100  .2200  .2200      0      0 -18.00      0  10.00];
        out = phantom3(e,20);
        
    end


end

